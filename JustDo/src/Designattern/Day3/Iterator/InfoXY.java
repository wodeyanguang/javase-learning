package Designattern.Day3.Iterator;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/21 20:51
 * @description： 信息学院里面的专业
 */
public class InfoXY implements XY {
    private List<ZY> zyList;
    private int index = -1;

    public InfoXY() {
        zyList = new LinkedList<>();
        addZY("语文", "不行");
        addZY("数学", "可以");
        addZY("英语", "一般");
    }

    @Override
    public String getname() {
        return "这是信息学院";
    }

    @Override
    public void addZY(String name, String info) {
        ZY zy = new ZY(name, info);
        zyList.add(zy);
        index++;
    }

    @Override
    public Iterator getZYTerator() {
        return new InfoIterator(zyList);
    }
}
