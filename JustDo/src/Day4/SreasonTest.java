package Day4;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/29 17:51
 * @description： 类的对象只有有限个，确定的。我们称此类为枚举类 当我们定义一组常量的时候，可以用枚举类
 * 还可以用enum关键字
 * 三、Enum类中的常用方法:
 * values()方法:返回枚举类型的对象数组。该方法可以很方便地遍历所有的枚举值。
 * valueOf(String str): 可以把-一个字符串转为对应的枚举类对象。要求字符串必须是枚举类破
 * toString():返回当前枚举类对象常量的名科I
 */
interface a{
    void show();
}

enum Season implements a{//写一个接口
    //    public static final Season SPRING = new Season("春","花开");
//    public static final Season SUMMER = new Season("夏","泳池");
//    public static final Season AUTUMN = new Season("秋","落叶");
//    public static final Season WINTER = new Season("冬","下雪");
    SPRING("春", "花开"){
        @Override
        public void show() {
            System.out.println("chun");
        }
    },
    SUMMER("夏", "泳池"){
        @Override
        public void show() {
            System.out.println("xia");
        }
    },
    AUTUMN("秋", "落叶"){
        @Override
        public void show() {
            System.out.println("qiu");
        }
    },
    WINTER("冬", "下雪"){
        @Override
        public void show() {
            System.out.println("dong");
        }
    };
    //常量也不能变
    private final String seasonname;
    private final String seasonDesc;

    //构造器需要私有化
    private Season(String seasonname, String seasonDesc) {
        this.seasonname = seasonname;
        this.seasonDesc = seasonDesc;
    }

    //提供对象，只用调用就好
    //只能提供get方法
    public String getSeasonname() {
        return seasonname;
    }

    public String getSeasonDesc() {
        return seasonDesc;
    }

    @Override
    public String toString() {
        return "Season{" +
                "seasonname='" + seasonname + '\'' +
                ", seasonDesc='" + seasonDesc + '\'' +
                '}';
    }
//    @Override
//    public void show() {
//        System.out.println("这是重写过后的方法");
//    }
}

public class SreasonTest {
    public static void main(String[] args) {
        Season spring = Season.SPRING;
        System.out.println(spring);//tostring方法
        System.out.println(Season.class.getSuperclass());

        //查看枚举里面的所有对象,都封装到一个数组里去了
        Season[] values = Season.values();
        for (int i = 0; i < values.length; i++) {
            System.out.println(values[i]);//tostring方法
            values[i].show();
        }

        System.out.println("------------------------");
        //返回对象名的描述
        Season winter = Season.valueOf("WINTER");
        System.out.println(winter);

    }
}
