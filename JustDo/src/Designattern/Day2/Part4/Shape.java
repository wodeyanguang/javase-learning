package Designattern.Day2.Part4;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/17 17:03
 * @description：
 */
public abstract class Shape {
    public abstract void draw();
}
