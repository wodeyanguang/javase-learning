package Designattern.Day3.Factroy.easyfactry;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/18 9:47
 * @description：
 */
public class Factory {
    public static Pizz createPizz(String pizztype) {
            Pizz pizz = null;
            if ("ChinaPizz".equalsIgnoreCase(pizztype)) {
                pizz = new ChinaPizz();
                pizz.setName("中国披萨");
            } else if ("USAPizz".equalsIgnoreCase(pizztype)) {
                pizz = new USAPizz();
                pizz.setName("美国披萨");
            } else {
                System.out.println("没有此类型的披萨");
            }
            return pizz;
        }
}
