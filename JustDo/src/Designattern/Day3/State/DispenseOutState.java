package Designattern.Day3.State;

/**
 * @author Administrator
 *
 */
public class DispenseOutState extends State {

	//初始化当前状态
    RaffleActivity activity;

    public DispenseOutState(RaffleActivity activity) {
        this.activity = activity;
    }
    @Override
    public void deductMoney() {
        System.out.println("奖品发送完！");
    }

    @Override
    public boolean raffle() {
        System.out.println("不能进行抽奖！");
        return false;
    }

    @Override
    public void dispensePrize() {
        System.out.println("奖品发送完！");
    }
}
