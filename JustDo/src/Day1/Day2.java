package Day1;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/26 19:52
 * @description：加入static就是静态变量，只有一份，所有对象一起共用
 */
class window extends Thread{
    private /**static**/ int ticket=100;

    @Override
    public void run() {
        while (true){
            if (ticket>0){
                System.out.println(getName()+"票号为:"+ticket);
                ticket--;
            }else {
                break;
            }

        }
    }
}


public class Day2 {
    public static void main(String[] args) {
//        window t1 = new window();
//        window t2 = new window();
//        window t3 = new window();
        windowagain w=new windowagain();//只造了一个对象，所以ticket用的是一个,相当于是static

        Thread t1 =new Thread(w);
        Thread t2 =new Thread(w);
        Thread t3 =new Thread(w);

        t1.setName("窗口一");
        t2.setName("窗口二");
        t3.setName("窗口三");

        t1.start();
        t2.start();
        t3.start();
    }
}
