package JDBC_1.preparedstatement;

import JDBC_1.ConnectionTest;
import JDBC_1.util.JDBCUtils;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Properties;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/11 20:30
 * @description： 完成增删改查的四种操作
 */
public class PreparedStatementUpdateTest {
    //向数据表中添加数据
    @Test
    public void testInsert() {
        Connection connection = null;
        PreparedStatement ps = null;//选择数据输入的语句
        try {
            //1.获取连接数据库test
            InputStream is = ConnectionTest.class.getClassLoader().getResourceAsStream("jdbc.properties");
            Properties pp = new Properties();
            pp.load(is);
            String user=pp.getProperty("user");
            String password=pp.getProperty("password");
            String url=pp.getProperty("url");
            String driverClass=pp.getProperty("driverClass");
            Class.forName(driverClass);
            connection = DriverManager.getConnection(url, user, password);

            //2.预编译sql语句，返回陈述实例
            String sql="insert into customers(name,email,birth)values(?,?,?)";//?：占位符
            ps = connection.prepareStatement(sql);

            //3.填充占位符
            ps.setString(1,"闫广");
            ps.setString(2,"YanGuang@gmail.com");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date date = sdf.parse("1000-01-01");
            ps.setDate(3,new Date(date.getTime()));

            //4.执行SQL
            ps.execute();
            System.out.println("插入成功！");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //5.资源关闭
            try {
                if (ps !=null)
                ps.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            try {
                if (connection!=null)
                connection.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
    //修改表的记录
    @Test
    public void testUpdate()  {
            //1.获取数据库的连接
            Connection connection = null;
            PreparedStatement ps = null;
        try {
            connection = JDBCUtils.getConnection();
            //2.获取带编译的sql语句
            String sql = "update customers set name = ? where id = ?";
            ps = connection.prepareStatement(sql);
            //3.填充占位符
            ps.setObject(1,"刘晟浩");
            ps.setObject(2,18);
            //4.执行
            ps.execute();
            System.out.println("修改成功！");
            //5.关闭
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            JDBCUtils.closeResourece(connection,ps);
        }
        //5.资源关闭
    }
    @Test
    public void testDelete(){
        InputStream is = null;
        PreparedStatement preparedStatement = null;
        try {
            //1.构建连接
            is = ClassLoader.getSystemClassLoader().getResourceAsStream("jdbc.properties");
            Properties ps = new Properties();
            ps.load(is);

            String user=ps.getProperty("user");
            String password=ps.getProperty("password");
            String url=ps.getProperty("url");
            String driverClass=ps.getProperty("driverClass");

            Class.forName(driverClass);
            Connection connection = DriverManager.getConnection(url, user, password);

            //2.编写sql预备语句
            String srl="delete from customers where id = ?";

            //3.写占位符
            preparedStatement = connection.prepareStatement(srl);
            preparedStatement.setObject(1,28);

            //4.执行语句
            preparedStatement.execute();
            System.out.println("删除成功！");

            //5.关闭
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement!=null)
                preparedStatement.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            try {
                if (is!=null)
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
    //通用的增删改查操作
    public void update(String sql,Object ...args){
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            //1.获取连接
            connection = JDBCUtils.getConnection();
            //2.预写sql
            ps = connection.prepareStatement(sql);
            //3.填充占位符
            for (int i = 0; i < args.length; i++) {
                ps.setObject(i+1,args[i]);//小心参数声明错误
            }
            //4.执行语句
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            //5.关闭
            JDBCUtils.closeResourece(connection,ps);
        }
    }
    @Test
    public void testUpdate2(){
        String sql= "update customers set `name` = ? where id = ?";//这里要注意有时候要加上``符号
        update(sql,"广广",28);
        System.out.println("操作成功！");
    }

}


























