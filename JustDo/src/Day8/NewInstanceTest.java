package Day8;

import org.junit.Test;

import java.util.Random;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/4 17:42
 * @description： 通过反射来创建类的对象
 *
 */
public class NewInstanceTest {
    @Test
    public void test1() throws IllegalAccessException, InstantiationException {

        /*
        要想此方法正常的创建运行时类的对象，要求:
        1.运行时类必须提供空参的构造器
        2.空参的构造器的访问权限得够。通常，设置为public

        在javabean中要求提供一个public的空参构造器。 原因:
        1.便于通过反射，创建运行时类的对象
        2.便于子类继承此运行时类时，默认调用super()时， 保证父类有此构造器

         */
        Class<Person> clazz = Person.class;//创建Class类的对象

        Person p = clazz.newInstance();//创建Person类的对象,调用的是空参构造器

        System.out.println(p);
    }

    //在框架中我们往往不知道需要造什么类，所以我们需要利用反射的动态性，以下就是例子：
    @Test
    public void test2(){
        for (int i = 0; i <10 ; i++) {
            int num = new Random().nextInt(3);//0.1.2
            String classPath = "";
            switch (num) {
                case 0:
                    classPath = "java.util.Date";
                    break;
                case 1:
                    classPath = "java.lang.Object";
                    break;
                case 2:
                    classPath = "Day8.Person";
                    break;
            }
            Object obj = null;
            try {
                obj = getInstance(classPath);
                System.out.println(obj);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    /*
    创建-一个指定类的对象。
    classPath:指定类的全类名
    */
    public Object getInstance(String classPath) throws Exception {
        Class clazz = Class. forName(classPath) ;
        return clazz.newInstance( );//只能调用空参构造器
    }

}
