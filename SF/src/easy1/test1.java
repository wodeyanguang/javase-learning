package easy1;

import org.junit.jupiter.api.Test;
import java.util.ArrayList;
/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/8 7:16
 * @description：
 */
public class test1 {
    @Test
//给你一个数组 candies 和一个整数 extraCandies ，其中 candies[i] 代表第 i 个孩子拥有的糖果数目。
//对每一个孩子，检查是否存在一种方案，将额外的 extraCandies 个糖果分配给孩子们之后，此孩子有 最多 的糖果。注意，允许有多个孩子同时拥有 最多 的糖果数目。
    public void  test1(){
        int[] candies={2, 3, 5, 1, 3};
        int extraCandies = 3 ;
        int max=candies[0];
        ArrayList<Boolean> list = new ArrayList<>();
        for (int i = 1; i < candies.length; i++) {
                if (max<candies[i]){
                    max=candies[i];
                }
            }
        for (int i :candies) {//增强for循环里的i指的就是每个的candies[i]
            list.add((i+extraCandies)>=max);
        }
        System.out.println(list);
    }
    @Test
    public void test2(){
//        字符串的左旋转操作是把字符串前面的若干个字符转移到字符串的尾部。请定义一个函数实现字符串左旋转操作的功能。比如，输入字符串"abcdefg"和数字2，该函数将返回左旋转两位得到的结果"cdefgab"。
        String s="abcdefg";
        char[] str=s.toCharArray();
        char[] strr=new char[s.length()];
        char[] strr1=new char[s.length()];
        int k=2;
        for (int i = s.length()-k,j=0; j < k; i++,j++) {
            strr1[i]=str[j];
        }
        for (int i = k; i <s.length();i++) {
            strr[i-k]=str[i];
        }
        for (int i = s.length()-k,j=0; j < k; i++,j++) {
            strr[i]=strr1[i];
        }
        String a = String.valueOf(strr);
        System.out.println(a);
//        优化算法:字符串拼接可以用stringbuilder
        StringBuilder sr=new StringBuilder();
        for (int i = k; i < s.length(); i++) {
            sr.append(s.charAt(i));
        }
        for (int i = 0; i < k; i++) {
            sr.append(s.charAt(i));
        }
        System.out.println(sr);
    }
    @Test
    public void test3(){
//        给你一个非负整数 num ，请你返回将它变成 0 所需要的步数。 如果当前数字是偶数，你需要把它除以 2 ；否则，减去 1 。
        int num =14;
        int tape=0;
        while (num>0) {
            if (num % 2 == 0 && num > 1) {
                num = num / 2;
                tape++;
            } else {
                num--;
                tape++;
            }
        }
        System.out.println(tape);
    }
    @Test
    public void test4(){
//        输入: J = "aA", S = "aAAbbbb"
//        输出: 3
        String S="aAAbbbb";
        String J="aA";
        int num=0;
        for (char s :S.toCharArray()) {
            for (char j : J.toCharArray()) {
                if (s==j){
                    num++;
                }
            }
        }
        System.out.println(num);
    }
    @Test
    public void test5(){
//        给你一个整数 n，请你帮忙计算并返回该整数「各位数字之积」与「各位数字之和」的差
        int n=234;
        int sub=1,num=0;
        while (n>0){
            sub*=n%10;
            num+=n%10;
            n = n / 10;
        }
        System.out.println(sub+"and"+num);
    }
    @Test
    public void test6(){
//        小A 和 小B 在玩猜数字。小B 每次从 1, 2, 3 中随机选择一个，小A 每次也从 1, 2, 3 中选择一个猜。他们一共进行三次这个游戏，请返回 小A 猜对了几次？
//        输入的guess数组为 小A 每次的猜测，answer数组为 小B 每次的选择。guess和answer的长度都等于3。
        int[] guess={2,2,3};
        int[] answer={3,2,1};
        int num=0;
        for (int i = 0; i <guess.length ; i++) {
                if (answer[i]==guess[i]){
                    num++;
            }
        }
        System.out.println(num);
    }
    @Test
    public void test7(){
        //求开根号，直接用sqrt就可以了，不用写算法
        int x=4;
        double sqrt = Math.sqrt(4);
        System.out.println(sqrt);
        if (x == 0) {
            System.out.println(0);
        }
        int ans = (int)Math.exp(0.5 * Math.log(x));
        System.out.println((long)(ans + 1) * (ans + 1) <= x ? ans + 1 : ans);

    }
}


