package Designattern.Day3.Single;
/*
    这个是最简单，而且问题都解决了的方式，极力推荐使用
    枚举里面直接写对象(属性)，默认静态
 */
public enum FinallySingle {
    SINGLE;
    public void ok(){
        System.out.println("Are You Ok ?");
    }

    public static void main(String[] args) {
        FinallySingle single = FinallySingle.SINGLE;
        single.ok();
    }
}
