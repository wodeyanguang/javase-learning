package Designattern.Day3.State;

/**
 * @author Administrator
 *不能抽奖的状态
 */
public class NoRaffleState extends State {

    //这个是当前的活动状态
    RaffleActivity activity;

    public NoRaffleState(RaffleActivity activity) {
        this.activity = activity;
    }

    @Override
    public void deductMoney() {
        System.out.println("你已经成功扣除50积分，可以抽奖");
        activity.setState(activity.getCanRaffleState());
    }

    @Override
    public boolean raffle() {
        System.out.println("扣了积分就可以抽奖");
        return false;
    }

    @Override
    public void dispensePrize() {
        System.out.println("不能发放奖品");
    }
}
