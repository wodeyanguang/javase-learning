package Designattern.Day3.theory;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/22 17:16
 * @description： 英雄
 */
public class Hero {
    private String state;

    public Hero(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Memento saveHeroMemento(){
        return new Memento(state);
    }

    public void rollbackHeroMemento(String state){
        this.state= state;
    }
}
