package Designattern.Day3.Single;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/18 8:48
 * @description： 静态内部类，也不错(同样推荐使用)
 * 当Staticsingle被装载的时候，她的内不类其实是没有被装载的，只有当使用getInstance方法的时候
 * 才会装载SingleValuse这个类
 */
public class Single4 {
    public static void main(String[] args) {
        Staticsingle instance = Staticsingle.getInstance();
        System.out.println(instance.hashCode());
    }
}

class Staticsingle{
    private Staticsingle() {
    }
    private static class SingleValuse{
        private static final   Staticsingle STATICSINGLE = new Staticsingle();
    }
    public static Staticsingle getInstance(){
        return SingleValuse.STATICSINGLE;
    }
}