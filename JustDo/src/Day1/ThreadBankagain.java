package Day1;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/27 17:22
 * @description：
 */
class Accounts{
    private double money;

    public Accounts(double amt) {
        money = amt;
    }
    public synchronized void addamt(double amt){
        if (amt>=0){
           money+=amt;
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"钱是"+money);
    }
}
class customer extends Thread{
    private Accounts acct;

    public customer(Accounts acct) {
        this.acct = acct;
    }

    @Override
    public void run() {
        for (int i = 0; i < 3; i++) {
            acct.addamt(1000);
        }
    }
}

public class ThreadBankagain {
    public static void main(String[] args) {
        Accounts accounts=new Accounts(0);

        customer a=new customer(accounts);
        customer b=new customer(accounts);

        a.setName("DaD");
        b.setName("MoM");

        a.start();
        b.start();
    }
}
