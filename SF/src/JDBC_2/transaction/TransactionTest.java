package JDBC_2.transaction;

import JDBC_1.util.JDBCUtils;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.sql.*;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/15 8:40
 * @description： 事务的回滚
 *
 * 1.什么叫数据库事务?
 * 事务:一组逻辑操作单元,使数据从一种状态变换到另种状态。
 * >一组逻辑操作单元:一个或多个DML操作。
 * 2.事务处理的原则:保证所有事务都作为一一个工作单元来执行，即使出现了故障，都不能改变这种执行方式。
 * 当在一个事务中执行多个操作时，要么所有的事务都被提交( commit),那么这些修改就永久地保存
 * 下来;要么数据库管理系统将放弃所作的所有修改，整个事务回滚(rollback )到最初状态。
 *
 * 3.数据一旦提交，就不可回滚
 *
 * 4.哪些操作会导致数据的自动提交?
 * >DDL操作（创建修改表）-旦执行， 都会自动提交。
 *      >set autocommit = false对DDL操作失效
 * >DML（增删查改）默认情况下，- -旦执行，就会自动提交。
 *      >我们可以通过set autocommit = false的方 式取消DML操作的自动提交。
 * >默认在关闭连接时，会自动的提交数据
 */
public class TransactionTest {
    //********************考虑事务后的转账操作*******************************
    @Test
    public void testUpdateWithTx(){
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            //1.取消数据的自动提交功能DML
            connection.setAutoCommit(false);
            String sql1 = "update user_table set balance = balance - 100 where user = ?";
            update(connection,sql1, "AA");

            //模拟异常，这样数据就回滚了，不会再异常了
            System.out.println(10/0);

            String sql2 = "update user_table set balance = balance + 100 where user = ?";
            update(connection,sql2, "BB");
            System.out.println("转账成功！");

            //2.提交数据
            connection.commit();
        } catch (Exception e) {
            e.printStackTrace();
            //3.回滚
            try {
                connection.rollback();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }finally {
            try {
                //主要针对数据库连接池的时候还需要恢复自动提交
                connection.setAutoCommit(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
            JDBCUtils.closeResourece(connection,null);
        }
    }
    //通用的增删改查操作---version 2.0
    public int update(Connection connection,String sql, Object... args) {
        PreparedStatement ps = null;
        try {
            //1.预写sql
            ps = connection.prepareStatement(sql);
            //2.填充占位符
            for (int i = 0; i < args.length; i++) {
                ps.setObject(i + 1, args[i]);//小心参数声明错误
            }
            //3.执行语句
            return ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //4.关闭
            JDBCUtils.closeResourece(null, ps);
        }
        return 0;
    }

    /*
    对于A,B两个账户需要转账，所以A--，B++这两部修改操作共同完成就叫做一个事务
     */
    @Test
    public void testUpdate() {
        String sql1 = "update user_table set balance = balance - 100 where user = ?";
        update(sql1, "AA");

        //模拟网络异常,AA已经花了100，BB没收到
        System.out.println(10 / 0);

        String sql2 = "update user_table set balance = balance + 100 where user = ?";
        update(sql2, "BB");
        System.out.println("转账成功！");
    }

    //通用的增删改查操作---version 1.0
    public int update(String sql, Object... args) {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            //1.获取连接
            connection = JDBCUtils.getConnection();
            //2.预写sql
            ps = connection.prepareStatement(sql);
            //3.填充占位符
            for (int i = 0; i < args.length; i++) {
                ps.setObject(i + 1, args[i]);//小心参数声明错误
            }
            //4.执行语句
            return ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //5.关闭
            JDBCUtils.closeResourece(connection, ps);
        }
        return 0;
    }
    //**************************************************
    @Test
    public void testTransctionSelect() throws Exception {
        Connection connection = JDBCUtils.getConnection();
        //获取当前连接的隔离级别
        System.out.println(connection.getTransactionIsolation());
        //设置数据库的隔离级别
        connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
        //取消自动提交
        connection.setAutoCommit(false);
        String sql="select user,password,balance from user_table where user = ?";
        User user = getInstance(connection, User.class, sql, "CC");
        System.out.println(user);
    }
    @Test
    public void testTransctionUpdate() throws Exception {
        Connection connection = JDBCUtils.getConnection();
        //取消自动提交
        connection.setAutoCommit(false);
        String sql="update user_table set balance = ? where user = ?";
        update(connection,sql,5000,"CC");
        Thread.sleep(5000);
        System.out.println("修改结束");
    }
    //统一的查询操作(vserion 2.0,考虑了事务进来)
    public <T> T getInstance(Connection connection,Class<T> clazz, String sql, Object... args) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                ps.setObject(i + 1, args[i]);
            }
            //执行，获得结果集
            rs = ps.executeQuery();
            //获取结果集的元数据
            ResultSetMetaData rsmd = rs.getMetaData();
            //获取字段据个数
            int columnCount = rsmd.getColumnCount();
            if (rs.next()) {
                //获取一个类的对象
                T t = clazz.newInstance();
                for (int i = 0; i < columnCount; i++) {
                    //获取每一个列的列值:通过resultset
                    Object columnValue = rs.getObject(i + 1);
                    //获取列的别名：getcolumnlabel（）推荐使用
                    String columnName = rsmd.getColumnLabel(i + 1);
                    //通过反射将对象指定属性名的属性赋值
                    Field field = clazz.getDeclaredField(columnName);
                    field.setAccessible(true);
                    field.set(t, columnValue);
                }
                return t;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResourece(null, ps, rs);
        }
        return null;
    }
}
