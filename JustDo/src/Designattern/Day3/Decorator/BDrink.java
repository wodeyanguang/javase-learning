package Designattern.Day3.Decorator;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/20 9:55
 * @description：
 */
public class BDrink extends Coffee{
   public BDrink(){
       setCost(2);
       setDsc("这是BCOffee");
   }
}
