package Designattern.Day3.Single;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/18 7:44
 * @description： 饿汉式（静态）
 * 可以用，但是每次都会创建一个对象，可能造成内存浪费
 * 就是说你只要创建了这个对象，内部就会自动的创建这个对象，造成浪费
 * 饿汉式就是一开始就很饿，然后直接给你创建好对象
 */
public class Single1 {
    public static void main(String[] args) {
        SingLe singLe = SingLe.getinstce();
        System.out.println(singLe.hashCode());
    }

}
class SingLe{
    private final static SingLe SING_LE=new SingLe();

    SingLe() {
    }
    public static SingLe getinstce(){
        return SING_LE;
    }
}
