package Day6.HomeWork;

import java.util.*;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/2 17:44
 * @description：泛型类 分别创建以下方法:
 * .public void save(String id,T entity):保存 T类型的对象到Map成员变量中
 * lpublic T get(String id):从map 中获取id对应的对象
 * public void update(String id,T entity): 替换map中key 为id的内容,改为entity 对
 * public List<T> list(): 返回map中存放的所有T对象
 * public void delete(String id):删除指定id对象
 */
public class DAO<T> {
    private Map<String,T> map =new HashMap<>();

    public void save(String id,T entity) {
        map.put(id,entity);
    }
    public T  get(String id) {
       return map.get(id);
    }
    public void update(String id,T entity) {
        if (map.containsKey(id)){
            map.put(id,entity);
        }
    }
    public List<T> list() {
//        错误的
//        Collection<T> values =map.values();
//        return (List<T>) values;
        ArrayList<T> arrayList=new ArrayList<>();
        Collection<T> collections=map.values();//获取map中的值
        for (T t :collections){
            arrayList.add(t);
        }
        return arrayList;
    }
    public void delete(String id) {
        map.remove(id);
    }

}
