package Day7;

import org.junit.Test;

import java.io.*;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/3 16:56
 * @description： 其他流的使用
 * 1.标准的输入、输出流
 * 2.打印流
 * 3.数据流
 */
public class OtherStreamTest {
    /*
    1.标准的输入、输出流
    System. in:标准的输入流，默认从键盘输入,字节型
    System. out:标准的输出流，默认从控制台输出，字节型
    1.2
    System类的setIn(InputStream is) / setout(PrintStream ps)方式重新指定输入和输出的式
    1.3练习:
    从键盘输入字符串，要求将读取到的整行字符串转成大写输出。然后继续进行输入操作，
    直至当输入“e”或者“exit”时，退出程序。

    2.打印流: PrintStream 和Printwriter
    打印流可以直接打印到文件当中
    2.1提供了一系列重载的print() 和println()

    3.数据流
    3.1 DataInputStream 和DataOutputStream
    3.2作用:用于读取或写出基本数据类型的变量或字符串

    将内存中的字符串写入文件中

序列化:
Person需要满足如下的要求，方可序列化
1.需要实现接口: Serializable
2.当前类提供-一个全局常量: serialVersionUID
3.除了当前Person类需要实现Serializable接口之外，还必须保证其内部所有属1
力以须县可文列化所。

    */
    @Test
    public void test1() {
        InputStreamReader isr = new InputStreamReader(System.in);//提示用ctrl+shift+空格,按F4查看调用关系
        BufferedReader br = new BufferedReader(isr);
    }

    @Test//数据流
    public void test2() throws IOException {
        DataOutputStream dos = new DataOutputStream(new FileOutputStream("data.txt"));
        dos.writeUTF("闫广");
        dos.flush();
        dos.writeInt(23);

        dos.close();
    }
}
