package JDBC_2.DAO;

import JDBC_1.bean.Customer;

import java.sql.Connection;
import java.util.Date;
import java.util.List;

public interface CustomerDAO {
    //将cust对象添加到数据库中
    void insert(Connection connection, Customer cust);
    //针对指定的id，删除表中的记录
    void deleteById(Connection connection,int id);
    //针对内存中的cust对象去修改数据表中的记录
    void updateById(Connection connection,Customer cust);
    //根据指定的id来查询对于对应的Customer
    Customer  getCustomerById(Connection connection,int id);
    //查询表中多条记录
    List<Customer> getAll(Connection connection);
    //返回count（*）
    Long getCount(Connection connection);
    //返回最大的生日
    Date getMaxBirth(Connection connection);
}
