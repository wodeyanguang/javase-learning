package Day6.HomeWork;

import java.util.List;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/2 18:02
 * @description：
 */
public class DAOTest {
    public static void main(String[] args) {
        DAO<User> dao =new DAO<>();

        dao.save("1001",new User(1001,34,"周杰伦"));
        dao.save("1002",new User(1002,20,"昆凌"));
        dao.save("1003",new User(1003,25,"蔡依林"));

        List<User> list = dao.list();
        list.forEach(System.out::println);//list JDK8遍历方法

        System.out.println("-------------------");
        dao.delete("1002");
        dao.update("1003",new User(1003,30,"方文山"));
        //更改删除操作
        List<User> list1 = dao.list();
        list1.forEach(System.out::println);//list JDK8遍历方法
    }
}
