package Designattern.Day3.theory;

import java.util.LinkedList;
import java.util.List;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/22 17:24
 * @description： 备忘录模式
 * 就是把一个数据给备忘一下然后方便下次的调用
 */
public class Creataker {
    List<Memento> mementos;
    int index=0;

    public Creataker() {
        mementos = new LinkedList<>();
    }

    public void addmemento(Memento memento){
     mementos.add(memento);
     index++;
    }

    public String seememento(int index){
        return mementos.get(index).getState();
    }

    public static void main(String[] args) {
        Creataker creataker = new Creataker();
        Hero hero;
        hero=new Hero("满血");
        creataker.addmemento(hero.saveHeroMemento());
        System.out.println(hero.getState()+"\t"+creataker.seememento(0));
        hero=new Hero("半血");
        creataker.addmemento(hero.saveHeroMemento());
        System.out.println(hero.getState()+"\t"+creataker.seememento(1));
        hero.rollbackHeroMemento(creataker.seememento(0));
        System.out.println(hero.getState());
    }
}
