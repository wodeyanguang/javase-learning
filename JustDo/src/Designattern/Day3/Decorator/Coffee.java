package Designattern.Day3.Decorator;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/20 9:52
 * @description：
 */
public class Coffee extends Drink{
    @Override
    public int getcose() {
        return getCost();
    }

    @Override
    public String getdscc() {
        return getDsc();
    }

}
