package Day4;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/29 22:07
 * @description：仅仅是内存表面的存储，不是持久化的存储
 * 数组在存储多个数据方面的缺点:
 * >一旦初始化以后，其长度就不可修改。
 * >数组中提供的方法非常有限，对于添加、删除、插入数据等操作,非常不便，同时效率不高。
 * >获取数组中实际元素的个数的需求，数组没有现成的属性或方法可用
 * >数组存储数据的特点:有序、可重复。对于无序、不可重复的需求，不能满足。
 *二、集合框架
 * /----Collection接口:单列集合，用来存储一个一个的对象
 * /----List接口:存储有序的、可重复的数据。-->*动态”数组
 * /----ArrayList、 LinkedList、 Vector
 * ----Set接口:存储无序的、不可重复的数据--> 高中讲的“集合”
 * I----HashSet、LinkedHashSet、 TreeSet
 * /----Map接口:双列集合，用来存储一一对(key - value) -一对的数据
 * -->高中函数: y = f(x)
 * /----HashMap、LinkedHashMap、 TreeMap、 Hashtable、 Properties
 *像collection类中加数据对象的，要求所在类要重写equals方法
 * //Collection中定义的方法
 */

class person{
    private String name;
    private int age;

    public person(String name, int age) {
        this.name = name;
        this.age = age;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public java.lang.String toString() {
        return "person{" +
                "name=" + name +
                ", age=" + age +
                '}';
    }
}
public class CollectionTest {
    public static void main(String[] args) {
        Collection coll = new ArrayList();
        //add(0bject e)装入的是类
        coll.add("aa");
        coll.add("bb");
        coll.add(123);//自动装箱

        System.out.println(coll.size());//获取个数

        coll.add(456);
        coll.add(789);
        coll.addAll(coll);//全部加入
        System.out.println(coll);

        System.out.println(coll.isEmpty());//false

        coll.clear();//清空

        System.out.println(coll);

       coll.add(new person("Tom",20));

        boolean s = coll.contains(456);//判断是否包含
        System.out.println(s);//false

        System.out.println(coll.contains(new person("Tom",20)));//false

        person yan = new person("Yan", 20);
        coll.add(yan);
        System.out.println(coll.contains(yan));//true

        coll.remove(123);//一出一个
        System.out.println(coll);

        System.out.println(coll.hashCode());//返回当前对象的hash值

        Object[] objects = coll.toArray();//从集合转化为数组
        for (int i = 0; i < objects.length; i++) {
            System.out.println(objects[i]);
        }

        List list = Arrays.asList(new String[]{"sdas","das","dasda"});
        System.out.println(list);//从数组到集合


    }
}
