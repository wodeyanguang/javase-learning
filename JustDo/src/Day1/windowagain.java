package Day1;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/27 16:13
 * @description：第二种方式来创建多线程
 * 讲共享数据进行同步代码块来处理synchronized
 */
public class windowagain implements Runnable {
    private static int ticket=100;
    Object object=new Object();
    @Override
    public void run() {
        while (true) {
            synchronized (object) {//同步监视器(锁(但是要求多个线程一定要共用同一把锁))，任何一个类的对象,单独运行这一个线程，其他线程都要在外面排队
                if (ticket > 0) {
                    System.out.println(Thread.currentThread().getName() + "票号为:" + ticket);
                    ticket--;
                } else {
                    break;
                }
            }
        }
    }
}
