package Day3;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/28 16:49
 * @description：string:字符串，使用一对”"“ 引起来表示。
 * 1. String声明为final的，不可被继承
 * 2. String实现了Serializable接口:表示字符串是支持序列化的。
 * 实现了Comparable接口:表示String可以比较大小
 * 3.String内部定义了final char[] value用于存储字符串数据
 * 4. String:代表不可变的字符序列。简称:不可变性。
 * 体现:
 * 5.通过字面量的方式(区别nuew)给一个字符串赋值，此时的字符串值声明在字符串常量池中
 *
 */
public class StringTest {
    public static void main(String[] args) {
        String s ="abc";//保存在常量池,目的是共享地址。
        String t=s.replace('a','d');//替换字母函数,虽然长度没变但是地址还是变了
        System.out.println(s+t);
        String s1=new String("javaEE");//保存在堆空间
        String s2=new String("javaEE");//创建了两个不同的对象，地址不同
        //使用new方法其实创建了两个对象一个是s1，s2，另一个是常量池里面的char[]数组
        if (s1==s2){
            System.out.println("yes");
        }else {
            System.out.println("flase");
        }
    }
}
