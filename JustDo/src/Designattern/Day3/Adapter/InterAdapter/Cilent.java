package Designattern.Day3.Adapter.InterAdapter;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/19 9:55
 * @description： 接口适配器
 * 抽象类只要重写了里面的非抽象方法，就可以实例化。(也叫匿名内部类)
 * 就相当于又造了一个非抽象类
 * 匿名内部类，不管是不是用抽象类还是非抽象类来写的，反正就是创建了一个新的类
 * 然后重写了你想要重写的方法，而且还可以使用原来的方法
 */
public class Cilent {
    public static void main(String[] args) {
        Abstrction abstrction = new Abstrction() {
            @Override
            public void function1() {
                super.function1();
                System.out.println("接口适配器重写之后的方法一");
            }
        };
        abstrction.function1();
        abstrction.function2();
    }
}
