package Designattern.Day1;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/17 14:59
 * @description：
 */
public class A implements interface1, interface2 {

    @Override
    public void test1() {
        System.out.println("IN  A");
    }

    @Override
    public void test2() {
        System.out.println("IN  B");
    }

    @Override
    public void test3() {
        System.out.println("IN  C");
    }
}
