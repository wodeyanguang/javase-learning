package Designattern.Day3.Iterator;

import java.util.Iterator;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/21 20:33
 * @description： 计算机学院里面的专业
 */
public class ComputerXY implements XY{
    private ZY[] zies;
    private int index =0;

    public ComputerXY() {
        zies = new ZY[5];
        addZY("物联网工程","不错");
        addZY("计算机科学与技术","很好");
        addZY("通信工程","不错");
    }

    @Override
    public String getname() {
        return "这里是计算机学院";
    }

    @Override
    public void addZY(String name, String info) {
        ZY zy = new ZY(name, info);
        zies[index]=zy;
        index++;
    }


    @Override
    public Iterator getZYTerator() {
        return new ComputerIterator(zies);
    }
}
