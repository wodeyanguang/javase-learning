package Designattern.Day3.Factroy.GoodFactory;

import Designattern.Day3.Factroy.GoodFactory.PizzBean.BJChinapizz;
import Designattern.Day3.Factroy.GoodFactory.PizzBean.BJUSApizz;
import Designattern.Day3.Factroy.GoodFactory.PizzBean.Pizzs;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/18 15:00
 * @description：
 */
public class BJFactory implements AbFactory{
    private Pizzs pizzs= null;
    @Override
    public Pizzs createPizzs(String type) {
        if ("BJChinapizz".equalsIgnoreCase(type)){
            pizzs = new BJChinapizz();
            pizzs.setname("BJChinapizz");
        }else if ("BJUSApizz".equalsIgnoreCase(type)){
            pizzs = new BJUSApizz();
            pizzs.setname("BJUSApizz");
        }else {
            System.out.println("没有这种类型的披萨");
        }
        return pizzs;
    }
}
