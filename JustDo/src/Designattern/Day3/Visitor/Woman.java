package Designattern.Day3.Visitor;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/21 15:03
 * @description：
 */
public class Woman extends Person{
    @Override
    void accpet(Action action) {
        action.getWomaninfo(this);
    }
}
