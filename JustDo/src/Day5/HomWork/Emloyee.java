package Day5.HomWork;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/30 20:28
 * @description：compare比较器
 */
public class Emloyee implements Comparable {//制造大众已有的比较方式
    private String name;
    private int age;
    private MyDate birthday;

    public Emloyee(String name, int age, MyDate birthday) {
        this.name = name;
        this.age = age;
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public MyDate getBirthday() {
        return birthday;
    }

    public void setBirthday(MyDate birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "Emloyee{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", birthday=" + birthday +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof  Emloyee){
            Emloyee e=(Emloyee)o;
            return this.name.compareTo(e.name);
        }
        throw new RuntimeException("传入数据不一致");
    }
}

