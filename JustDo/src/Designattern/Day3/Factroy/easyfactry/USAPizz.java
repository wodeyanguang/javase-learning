package Designattern.Day3.Factroy.easyfactry;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/18 9:46
 * @description：
 */
public class USAPizz extends Pizz{

    @Override
    public void process() {
        System.out.println("正在加工美国披萨");
    }
}
