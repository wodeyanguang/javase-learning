package Day3;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/29 6:22
 * @description：int Length(): 返回字符串的长度: return value. Length
 * char charAt(int index):返回某索 引处的字符return value[index]
 * boolean isEmpty(): 判断是否是空字符串: return value. Length ==日
 * String tolowerCase(): 使用默认语言环境，将String中的所有字符转换为小写
 * String toUpperCase(): 使用默认语言环境，将String中的所有字符转换为大写
 * String trim(): 返回字符串的副本，忽略前导空白和尾部空白
 * boolean
 * equals(Object obj):比较字符串的内容是否相同
 * boolean equalsIgnoreCase(String anotherString): 与equals方法类似， 忽略大小写
 * String concat(String str):将指定字符串连接到此字符串的结尾。等价于用“+”
 * int compareTo(String anotherString): 比较两个字符串的大小
 * String substring(int beginIndex): 返回一个新的字符串，它是此字符串的从beginIndex
 * String substring(int beginIndex, int endIndex) :返回一一个新字符串，它是此字符串，从开始到结束
 */
public class StringTest4 {
    public static void main(String[] args) {
        String s1="helloworld";
        System.out.println(s1.charAt(0));
        System.out.println(s1.isEmpty());
        String s2 = s1.toUpperCase();//string是不可变的，所以要新令对象
        System.out.println(s2);

        String s3="   hello  wor  ld    ";
        String s4=s3.trim();
        System.out.println("--------"+s3+"--------------");
        System.out.println("--------"+s4+"--------------");

        String s5="abc";
        String s6=new String("abd");//aclls码
        System.out.println(s5.compareTo(s6));

        String s7="背景图案";
        String s8=s7.substring(2);
        String s9=s7.substring(2,3);
        System.out.println(s8+"-------"+s9);
    }
}
