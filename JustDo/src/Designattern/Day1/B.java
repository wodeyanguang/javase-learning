package Designattern.Day1;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/17 15:00
 * @description： B依赖于A ，调用实现类的形参的时候就用interface
 */
public class B {
    public void depande1(interface1 i) {//这里是输入接口的实现类，也就是接口的对象
        i.test1();
    }

    public void depande2(interface1 i) {
        i.test2();
    }

    public void depande3(interface2 i) {
        i.test3();
    }

    public static void main(String[] args) {
        B b = new B();
        //这里指的就是B类依赖于接口实现类A（所以实现类A也可以算作接口的对象）
        b.depande1(new A());
        b.depande2(new A());
        b.depande3(new A());
        System.out.println();
        A a = new A();
        a.test1();
        a.test2();
        a.test3();
    }
}

