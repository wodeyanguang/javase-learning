package PrepareDay;

/**
 * @program: Demo2
 * @description:内部类的调用
 * @author: yan guang
 * @create: 2020-04-19 09:49
 */
public class NeiBuNei {
    public static void main(String[] args) {
        Person.Cat n1=new Person.Cat();
        n1.run();
        Person n2=new Person();
        Person.Dog n3= n2.new Dog();
        n3.chi();
    }
}

class Person {
    public void wan(){
        System.out.println("我是人");
    }
    class Dog{
        public void chi(){
            System.out.println("我是狗");
        }
    }
    static class   Cat{
        public static void  run(){
            System.out.println("我是猫");
        }
    }
}
