package Designattern.Day3.State;

import java.util.Random;

/**
 * @author Administrator
 *  可以抽奖的状态
 */
public class CanRaffleState extends State {

    RaffleActivity activity;

    public CanRaffleState(RaffleActivity activity) {
        this.activity = activity;
    }

    @Override
    public void deductMoney() {
        System.out.println("已经扣过积分");
    }

    @Override
    public boolean raffle() {
        System.out.println("正在抽奖，请等等！");
        Random r = new Random();
        int num = r.nextInt(10);
        // 百分之10的中奖机会
        if(num == 0){
            // 改变活动状态为发放奖品
            activity.setState(activity.getDispenseState());
            return true;
        }else{
            System.out.println("很遗憾您没有抽奖！");
            //改变状态为不能抽奖
            activity.setState(activity.getNoRafflleState());
            return false;
        }
    }

    @Override
    public void dispensePrize() {
        System.out.println("没有中奖，不能发放奖品！");
    }
}
