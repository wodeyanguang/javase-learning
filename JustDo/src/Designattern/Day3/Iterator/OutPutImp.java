package Designattern.Day3.Iterator;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/22 8:29
 * @description： 迭代器模式      一般用于不同数据存储类型的遍历
 * 首先创造一个类（zy）就是专门来实现迭代操作的，只负责遍历
 * 然后创造一个类（xy）就是专门来真正存储数据的类，然后遍历的时候就用getItertor方法就可以了
 * List已经实现了itertor接口
 */
public class OutPutImp {
    private List<XY> xyList;

    public OutPutImp(List<XY> xyList) {
        this.xyList = xyList;
    }

    public void OutPutXY(){
        Iterator<XY> iterator = xyList.iterator();
        while (iterator.hasNext()){
            XY xy = iterator.next();
            System.out.println("==========="+xy.getname()+"===========");
            Iterator zyTerator = xy.getZYTerator();
            OutPutZY(zyTerator);
        }
    }

    public void OutPutZY(Iterator iterator){
        while (iterator.hasNext()){
            ZY zy = (ZY) iterator.next();
            System.out.println(zy.getName()+zy.getInfo());
        }
    }

    public static void main(String[] args) {
        List<XY> xies=new LinkedList<>();
        InfoXY infoXY = new InfoXY();
        ComputerXY computerXY = new ComputerXY();
        xies.add(infoXY);
        xies.add(computerXY);

        OutPutImp outPutImp = new OutPutImp(xies);
        outPutImp.OutPutXY();
    }
}
