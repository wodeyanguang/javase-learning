package JDBC_1.preparedstatement;

import JDBC_1.bean.Order;
import JDBC_1.util.JDBCUtils;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.sql.*;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/13 8:53
 * @description： 针对于Order的查询操作
 */
public class OrderForQuery {
    @Test
    public void testorderForQuery(){
        String sql = "select order_id orderId,order_name orderName,order_date orderDate from `order` where order_id = ?";
        Order order=orderForQuery(sql,1);
        System.out.println(order);
    }
    /*
    *
    * 针对于order的表查询操作
    * 针对于表的字段名跟属性名不一致的情况，我们就用别名，而且加入你没写别名，别名默认就是字段名
    * 用getColumnLabel
    * */
    public Order orderForQuery(String sql,Object ...args){
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = JDBCUtils.getConnection();
            ps = connection.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                ps.setObject(i+1,args[i]);
            }
            //执行，获得结果集
            rs = ps.executeQuery();
            //获取结果集的元数据
            ResultSetMetaData rsmd = rs.getMetaData();
            //获取字段据个数
            int columnCount = rsmd.getColumnCount();
            if (rs.next()){
                Order order=new Order();
                for (int i = 0; i < columnCount; i++) {
                    //获取每一个列的列值:通过resultset
                    Object columnValue = rs.getObject(i + 1);
                    //获取每一个类的列名：通过resultsetmetadate
                    //获取列名：getcolumnname（）
                    //获取列的别名：getcolumnlabel（）推荐使用
                    String columnName = rsmd.getColumnLabel(i + 1);
                    //通过反射将对象指定属性名的属性赋值
                    Field field = Order.class.getDeclaredField(columnName);
                    field.setAccessible(true);
                    field.set(order,columnValue);
                }
                return order;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            JDBCUtils.closeResourece(connection,ps,rs);
        }
        return null;
    }


    @Test
    public void testQuery1(){
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = JDBCUtils.getConnection();

            String sql="select order_id,order_name,order_date from `order`where order_id = ?";

            ps = connection.prepareStatement(sql);

            ps.setObject(1,1);

            rs = ps.executeQuery();
            if (rs.next()){
                int id  = (int) rs.getObject(1);
                String name = (String)rs.getObject(2);
                Date date=(Date)rs.getObject(3);

                Order order = new Order(id, name, date);
                System.out.println(order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            JDBCUtils.closeResourece(connection,ps,rs);
        }
    }
}
