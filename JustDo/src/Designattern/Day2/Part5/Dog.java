package Designattern.Day2.Part5;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/17 17:22
 * @description： 子类需要重写父类的构造器,
 * 然后传递自己的一个参数进去，调用父类的构造器
 */
public class Dog extends Animal{
    public Dog(String name) {
        //这里指的就是把自己的名字传到父类那里去了，然后就可以调用父类的所有方法
        super(name);
    }

    public static void main(String[] args) {
        Dog dog = new Dog("柯基");
        dog.run();
    }
}
