package Day6;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/2 11:29
 * @description：Collections :操作Collection. Map的工具类
 * <p>
 * 面试题: Collection 和Collections的区别?
 * 前者是一个基本接口，后者是工具类
 */
public class CollectionsTest {
//   ● 排序操作: (均 为static方法)
//➢reverse(List):反转List中元素的顺序
//➢shuffle(List):对List集合元素进行随机排序
//➢sort(List):根据元素的自然顺序对指定List集合元素按升序排序
//➢sort(List，Comparator): 根据指定的Comparator产生的顺序对List 集合元素进行排序
//➢swap(List, int, int): 将指定list 集合中的i处元素和j处元素进行交换
//    查找、替换
//●Object max(Collection):根据元素的自然顺序，返回给定集合中的最大元素
//●Object max(Collection，Comparator): 根据Comparator指定的顺序，返回
//给定集合中的最大元素
//●Object min(Collection)
//●Object min(Collection, Comparator)
//●int frequency(Collection, Object): 返回指定集合中指定元索的出现次数
//●void copy(List dest,List src):将src中 的内容复制到dest中
//●boolean replaceAll(List list, Object oldVal, Object newVal):使用新值替换
// List对象的所有旧值


    @Test
    public void test1() {
        List list = new ArrayList();
        list.add(124);
        list.add(82);
        list.add(57);
        list.add(6234);

        System.out.println(list);

        Collections.reverse(list);
        System.out.println(list);

        Collections.shuffle(list);
        System.out.println(list);

        Collections.sort(list);
        System.out.println(list);

        Collections.swap(list, 1, 3);
        System.out.println(list);

//        collections类中提供了多个synchronizedXxx()方法，
//        该方法可使将指定集合包装成线程同步的集合，从而可以解决
//        多线程并发访问集合时的线程安全问题
//        返回的List1即为线程安全的List
        List list1 = Collections.synchronizedList(list);
    }
}
