package JDBC_2.DAO;

import JDBC_1.bean.Customer;
import JDBC_1.util.JDBCUtils;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.Date;
import java.util.List;

class CustomerDAOImpTest {

  private  CustomerDAOImp dao=new CustomerDAOImp();

    @Test
    void insert(){
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Customer cust = new Customer(1, "小飞", "324@qq", new Date(43536435L));
            dao.insert(connection,cust);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            JDBCUtils.closeResourece(connection,null);
        }

    }

    @Test
    void deleteById() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            dao.deleteById(connection,12);

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            JDBCUtils.closeResourece(connection,null);
        }

    }

    @Test
    void updateById() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Customer cust = new Customer(19, "abc", "beiduofen", new Date(2342342L));
            dao.updateById(connection,cust);

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            JDBCUtils.closeResourece(connection,null);
        }
    }

    @Test
    void getCustomerById() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Customer cust = dao.getCustomerById(connection, 33);
            System.out.println(cust);

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            JDBCUtils.closeResourece(connection,null);
        }
    }

    @Test
    void getAll() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            List<Customer> list = dao.getAll(connection);
            list.forEach(System.out::println);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            JDBCUtils.closeResourece(connection,null);
        }
    }

    @Test
    void getCount() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Long count = dao.getCount(connection);
            System.out.println(count);

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            JDBCUtils.closeResourece(connection,null);
        }
    }

    @Test
    void getMaxBirth() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();


        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            JDBCUtils.closeResourece(connection,null);
        }
    }
}