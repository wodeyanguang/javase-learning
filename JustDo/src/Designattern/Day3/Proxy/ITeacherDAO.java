package Designattern.Day3.Proxy;

public interface ITeacherDAO {
     void teach();
}
