package JDBC_3;

import org.apache.commons.dbcp.BasicDataSource;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/16 9:02
 * @description： 测试DBCP的数据库连接池技术
 */
public class DBCPTest {
    @Test
    public void testGetConnction() throws SQLException {
        //创建了一个DBCP的数据库连接池
        BasicDataSource basicDataSource = new BasicDataSource();
        //设置基本信息
        basicDataSource.setDriverClassName("com.mysql.jdbc.Driver");
        basicDataSource.setUrl("jdbc:mysql:///test");
        basicDataSource.setUsername("root");
        basicDataSource.setPassword("12345678");
        //设置相关的数据库配置
        basicDataSource.setInitialSize(10);
        basicDataSource.setMaxActive(10);

        Connection connection = basicDataSource.getConnection();
        System.out.println(connection);
    }
}
