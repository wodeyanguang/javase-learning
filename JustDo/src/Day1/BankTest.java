package Day1;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/27 16:55
 * @description：使用同步机制将单例模式中的懒汉式改写成线程安全
 */
public class BankTest {
}
class Bank{
    private Bank(){};
    private static  Bank instance =null;

    public static Bank getInstance(){
        if(instance == null){//如果锁被抢走了，后面的线程直接拿着数据走，不用进入同步浪费时间
        synchronized (Bank.class) {
            if (instance == null) {
                instance = new Bank();
            }
        }
        }
        return instance;
    }
}
