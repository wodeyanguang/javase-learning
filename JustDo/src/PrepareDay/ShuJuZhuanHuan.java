package PrepareDay;

/**
 * @program: Demo2
 * @description:基本类型，包类型和string类型的转换
 * @author: yan guang
 * @create: 2020-04-18 10:28
 */
public class ShuJuZhuanHuan {
    public static void main(String[] args) {
        int n1 = 10;
        Integer m1 = n1;
        System.out.println(m1.toString());//自动包装基本数据类型,toString自动省略掉了，通常都是自动调用
        int n2 = 15;
        Integer m2 = new Integer(n2);
        System.out.println(m2.toString());//基本数据类型转换成包装型
        float n3=22.8f;
        Float n4 =new Float(n3);
        String m3=String.valueOf(n3);
        String m4=String.valueOf(n4);
        System.out.println(n3 +"****"+n4);//基本数据类型和包装类型都可以转换成string类型
        String n5="1234";
        int m5=Integer.parseInt(n5);
        System.out.println(m5);//sting类型转换成基本数据类型
        //总结就是你要扎转换成什么就在哪里面去找
    }
}
