package JDBC_3;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.DataSources;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/15 18:31
 * @description：
 */
public class C3P0Test {
    //方式一：
    @Test
    public void testGetConnetion() throws Exception {
        //获取出c3p0数据库连接池
        ComboPooledDataSource cpds = new ComboPooledDataSource();
        cpds.setDriverClass("com.mysql.jdbc.Driver"); //loads the jdbc driver
        cpds.setJdbcUrl("jdbc:mysql://localhost:3306/test");
        cpds.setUser("root");
        cpds.setPassword("12345678");

        //设置初始时数据库当中的连接数
        cpds.setInitialPoolSize(10);
        //获取到了连接池当中的一个连接
        Connection connection = cpds.getConnection();
        System.out.println(connection);

        //关闭连接池
        DataSources.destroy(cpds);
    }
    //方式二：使用配置文件
    @Test
    public void testGetConnection1() throws SQLException {
        ComboPooledDataSource cpds = new ComboPooledDataSource("helloc3p0");
        Connection connection = cpds.getConnection();
        System.out.println(connection);
    }
}