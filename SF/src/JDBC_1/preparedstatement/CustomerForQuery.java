package JDBC_1.preparedstatement;

import JDBC_1.bean.Customer;
import JDBC_1.util.JDBCUtils;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.sql.*;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/13 8:08
 * @description： 仅仅针对Customer表的单表查询
 */
public class CustomerForQuery {
    @Test
    public void testqueryForCustomers(){
        String sql = "select id,name,birth,email from customers where id = ?";
        Customer customer = queryForCustomers(sql, 13);
        System.out.println(customer);

        String sql1 = "select id,name from customers where name = ?";
        Customer customer1 = queryForCustomers(sql1, "周杰伦");
        System.out.println(customer1);
    }

    /*
    * 针对于Customers表的通用查找
    * */
    public Customer queryForCustomers(String sql,Object ...args){
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = JDBCUtils.getConnection();

            ps = connection.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                ps.setObject(i+1,args[i]);
            }

            rs = ps.executeQuery();
            //获取结果集的元数据，就是select后面的属性
            ResultSetMetaData rsmd = rs.getMetaData();
            //通过rsmd来获取属性的列数
            int columnCount = rsmd.getColumnCount();
            if (rs.next()){
                Customer cust=new Customer();
                //处理机国际每一行数据中的每一个列
                for (int i = 0; i < columnCount; i++) {
                    //获取列值
                    Object columnValue = rs.getObject(i + 1);

                    //获取列名
                    String columnName = rsmd.getColumnName(i + 1);

                    //给Customer每个属性输入上value

                    Field field = Customer.class.getDeclaredField(columnName);
                    field.setAccessible(true);
                    field.set(cust,columnValue);
                }
                return cust;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            JDBCUtils.closeResourece(connection,ps,rs);
        }
        return  null;
    }

    @Test
    public void testQuery(){
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try {
            connection = JDBCUtils.getConnection();

            String sql = "select id,name,email,birth from customers where id = ?";

            ps = connection.prepareStatement(sql);
            ps.setObject(1,1);

            //执行,并返回结果集
            resultSet = ps.executeQuery();

            //处理结果集
            if (resultSet.next()){//next（）判断结果集下一跳是否有数据，如果有数据返回true，并且指针下移
                //获取该条数据各个字段的值
                int id = resultSet.getInt(1);
                String name = resultSet.getString(2);
                String email = resultSet.getString(3);
                Date birth = resultSet.getDate(4);

                //方式一：
    //            System.out.println("id="+id+",name="+name+"email="+email+",birth="+birth);

                //方式二：将数据封装成一个对象
                Customer customer = new Customer(id, name, email, birth);
                System.out.println(customer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            //关闭资源：
            JDBCUtils.closeResourece(connection,ps,resultSet);
        }
    }

}
