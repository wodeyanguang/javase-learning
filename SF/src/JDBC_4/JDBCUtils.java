package JDBC_4;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/16 8:12
 * @description： 使用c3p0的方式获取连接
 */
public class JDBCUtils {//new需要放在上面，不然每次使用连接池都要创造一个池子
    private static ComboPooledDataSource cpds = new ComboPooledDataSource("helloc3p0");
    public static Connection getConnctionByPool() throws SQLException {
        Connection connection = cpds.getConnection();
        return connection;
    }
}
