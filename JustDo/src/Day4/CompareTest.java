package Day4;

import java.util.Arrays;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/29 10:49
 * @description：Compare有默认，也可以自定义重写
 * compareTo可以直接比较两个Strin类型的大小，但是我们也需要一般重写
 * 需要比较各种属性的大小
 * Comparable接口与Comparator的使用的对比:
 * Comparable接口的方式-一旦一定，保证Comparable 接口实现类的对象在任何位置都可以比较大小
 * Comparator接口属于临时性的比较。 I
 */
class Good implements Comparable{
    private String name;
    private int price;

    public Good(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Good{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
    //指明自定义的比较大小方式
    @Override
    public int compareTo(Object o) {//重写Arrays中的compareTo类
        if (o instanceof Good){
            Good good=(Good) o;
//            if (this.price>good.price){
//                return 1;
//            }else if (this.price<good.price){
//                return -1;
//            }else {
//                return 0;
//            }
            return -Integer.compare(this.price,good.price);//返回一个大的,加一个负号，排序相反
        }
        throw new RuntimeException("传入数据类型不一致");
    }
}

public class CompareTest {
    public static void main(String[] args) {
        String[] arr = new String[]{"AA","CC", "KK", "MM", "GG","JJ", "DD"};
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));

        Good[] arrs = new Good[4];
        arrs[0] = new Good("lenovoMouse",34);
        arrs[1] = new Good( "del lMouse",43);
        arrs[2] = new Good( "xiaomiMouse",12);
        arrs[3] = new Good( "huaweiMouse",65);
        Arrays.sort(arrs);
        System.out.println(Arrays.toString(arrs));

        System.out.println(arrs[2].compareTo(arrs[0]));//重写的compareto
    }
}
