package Designattern.Day3.Factroy.GoodFactory.PizzBean;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/18 14:48
 * @description：
 */
public abstract class Pizzs {
    protected String name;

    public abstract void Prepared();

    public void run() {
        System.out.println(name + "正在路上");
    }

    public void setname(String name) {
        this.name = name;
    }
}
