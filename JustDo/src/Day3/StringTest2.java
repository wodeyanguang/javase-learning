package Day3;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/28 22:01
 * @description：string常量池与堆
 */
public class StringTest2 {
    public static void main(String[] args) {
        String s1 = "javaEE";
        String s2 = "hadoop";
        String s3 = "javaEEhadoop";//自变量
        String s4 = "javaEE" + "hadoop";//自变量连接，所以是一样的
        String s5 = s1 + "hadoop";//有变量值参与（s1），都不是常量值了，有点相当于new，在堆空间中开辟空间
        String s6 = "javaEE" + s2;
        String s7=s1+s2;

        System.out.println(s3 == s4);//true
        System.out.println(s3 == s5);//false
        System.out.println(s3 == s6);//false
        System.out.println(s5 == s6);//false
        System.out.println(s5 == s7);//false
        System.out.println(s6 == s7);//false

        String s8=s5.intern();//返回值得到的s8使用的常量值
        System.out.println(s8 == s3);//true
        //就是使用intern方法不管怎么样都可以把堆对象的常量池里的数给找出来
    }
}
