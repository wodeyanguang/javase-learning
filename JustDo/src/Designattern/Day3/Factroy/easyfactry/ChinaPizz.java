package Designattern.Day3.Factroy.easyfactry;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/18 9:45
 * @description：
 */
public class ChinaPizz extends Pizz{

    @Override
    public void process() {
        System.out.println("正在加工中国披萨");
    }
}
