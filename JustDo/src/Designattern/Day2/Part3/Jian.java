package Designattern.Day2.Part3;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/17 16:45
 * @description： 这里就是说让子类继承父类的时候尽量不要重写方法，尽量就是
 * 自己可以同时继承与一个更基础的父类，然后自己定义方法之后进行组合就可以了
 */
public class Jian extends base{
    private Jia jia=new Jia();
    public int jian(int a, int b){
        return a-b;
    }
    public int jiade(int a,int b){
        return jia.jia(a,b);
    }

    public static void main(String[] args) {
        Jian jian = new Jian();
        int jiade = jian.jiade(10, 2);
        int jian1 = jian.jian(10, 2);
        System.out.println(jiade+"\t"+jian1);
    }
}
