package Day1;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/27 17:08
 * @description：写一个多线程小项目甲乙共享一个账户，然后进行存钱
 */
class Account {
    private double balance;

    public Account(double balance) {
        this.balance = balance;
    }

    public synchronized void addmoney(double amt) {//解决安全问题，只能一个一个的存钱
        if (amt > 0) {
            balance += amt;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "余额为" + balance);
        }
    }
}

class Customer extends Thread {
    private Account account;

    public Customer(Account account) {
        this.account = account;
    }

    @Override
    public void run() {
        for (int i = 0; i < 3; i++) {
            account.addmoney(1000);
        }
    }
}

public class ThreadBank {
    public static void main(String[] args) {
        Account account = new Account(0);
        Customer c1 = new Customer(account);
        Customer c2 = new Customer(account);

        c1.setName("甲");
        c2.setName("乙");

        c1.start();
        c2.start();

    }
}
