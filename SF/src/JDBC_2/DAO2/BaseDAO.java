package JDBC_2.DAO2;

import JDBC_1.util.JDBCUtils;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/15 12:02
 * @description： 封装了对于数据表通用的增删改查操作
 * 不能用于实例化，就是一些方法
 *
 *
 * 有一个重要的知识点，获取当前子类调的父类泛型中的类型（class）
 */
public abstract class BaseDAO<T> {
    private Class<T> clazz =null;
//    public BaseDAO(){
//
//    }
{
    //这里的this.不是指的这个父类，是你在创造对象的时候的那个类，这里指的也就是DAOimp类
    //就相当于我们创建对象然后构造器赋值的时候，this.什么什么的都是对调用的这个对象，而不是super的对象
    Type genericSuperclass = this.getClass().getGenericSuperclass();//获取当前对象带泛型的父类
    ParameterizedType paramType = (ParameterizedType) genericSuperclass;
    Type[] typeArguments = paramType.getActualTypeArguments();//获取了父类泛型参数
    clazz= (Class<T>) typeArguments[0];//获取到了泛型的第一个参数，也就是Customer
}

    //通用的增删改查操作---version 2.0
    public int update(Connection connection, String sql, Object... args) {
        PreparedStatement ps = null;
        try {
            //1.预写sql
            ps = connection.prepareStatement(sql);
            //2.填充占位符
            for (int i = 0; i < args.length; i++) {
                ps.setObject(i + 1, args[i]);//小心参数声明错误
            }
            //3.执行语句
            return ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //4.关闭
            JDBCUtils.closeResourece(null, ps);
        }
        return 0;
    }

    //统一的查询操作(vserion 2.0,考虑了事务进来)
    public  T getInstance(Connection connection,  String sql, Object... args) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                ps.setObject(i + 1, args[i]);
            }
            //执行，获得结果集
            rs = ps.executeQuery();
            //获取结果集的元数据
            ResultSetMetaData rsmd = rs.getMetaData();
            //获取字段据个数
            int columnCount = rsmd.getColumnCount();
            if (rs.next()) {
                //获取一个类的对象
                T t = clazz.newInstance();
                for (int i = 0; i < columnCount; i++) {
                    //获取每一个列的列值:通过resultset
                    Object columnValue = rs.getObject(i + 1);
                    //获取列的别名：getcolumnlabel（）推荐使用
                    String columnName = rsmd.getColumnLabel(i + 1);
                    //通过反射将对象指定属性名的属性赋值
                    Field field = clazz.getDeclaredField(columnName);
                    field.setAccessible(true);
                    field.set(t, columnValue);
                }
                return t;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResourece(null, ps, rs);
        }
        return null;
    }

    //查询多行version 2.0
    public  List<T> getForList(Connection connection, String sql, Object... args) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                ps.setObject(i + 1, args[i]);
            }
            //执行，获得结果集
            rs = ps.executeQuery();
            //获取结果集的元数据
            ResultSetMetaData rsmd = rs.getMetaData();
            //获取字段据个数
            int columnCount = rsmd.getColumnCount();
            //创建集合对象
            ArrayList<T> list = new ArrayList<>();
            while (rs.next()) {
                //获取一个类的对象
                T t = clazz.newInstance();
                //给T对象复制的过程
                for (int i = 0; i < columnCount; i++) {
                    //获取每一个列的列值:通过resultset
                    Object columnValue = rs.getObject(i + 1);
                    //获取列的别名：getcolumnlabel（）推荐使用
                    String columnName = rsmd.getColumnLabel(i + 1);
                    //通过反射将对象指定属性名的属性赋值
                    Field field = clazz.getDeclaredField(columnName);
                    field.setAccessible(true);
                    field.set(t, columnValue);
                }
                list.add(t);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResourece(null, ps, rs);
        }
        return null;
    }
    //用于查询特殊值的方法
    public <E> E getValue(Connection connection,String sql,Object ...args){
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                ps.setObject(i+1,args[i]);
            }
            rs = ps.executeQuery();

            if (rs.next()){
                return (E) rs.getObject(1);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            JDBCUtils.closeResourece(null,ps,rs);
        }
        return null;
    }
}
