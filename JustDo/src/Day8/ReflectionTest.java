package Day8;

import org.junit.Test;

import java.lang.annotation.ElementType;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/4 10:08
 * @description： 反射
 */
public class ReflectionTest {
    //反射之前对于Person的操作
    @Test
    public void test1() {
        //1.创建对象
        Person p1 = new Person("Tom", 12);//只能调用public的构造器

        //2.调用内部属性跟方法
        p1.age = 10;//因为类中的age写的是public，所以可以直接重新赋值
        System.out.println(p1.toString());

        p1.show();

        //在person外部是不可以调用私有结构的，方法跟属性以及构造器都不行（封装性）
    }

    //有了反射之后,可以调用私有的一切
    @Test
    public void test2() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException {
        Class<Person> clazz = Person.class;
        //通过反射创建对象
        Constructor<Person> cons = clazz.getConstructor(String.class, int.class);
        Object obj = cons.newInstance("Tom", 12);
        Person p = (Person) obj;
        System.out.println(p.toString());

        //通过反射来调属性
        Field age = clazz.getDeclaredField("age");
        age.set(p, 10);
        System.out.println(p.toString());

        //调用方法
        Method show = clazz.getDeclaredMethod("show");
        show.invoke(p);
        //调用构造器
        Constructor<Person> cons1 = clazz.getDeclaredConstructor(String.class);
        cons1.setAccessible(true);
        Person p1 = (Person) cons1.newInstance("Jerry");
        System.out.println(p1);

        Field name = clazz.getDeclaredField("name");
        name.setAccessible(true);
        name.set(p1, "HanMEIMEI");
        System.out.println(p1);

        Method shownation = clazz.getDeclaredMethod("showNation", String.class);
        shownation.setAccessible(true);
        String nation = (String) shownation.invoke(p1, "中国");//有返回值就是形参
        System.out.println(nation);
    }
    //疑问1:通过直接new的方式或反射的方式都可以调用公共的结构，开发中到底用那个?
    //建议:直接new的方式。
    //什么时候会使用:反射的方式。反射的特征: ******动态性********
    //疑问2:反射机制与面向对象中的封装性是不是矛盾的?如何看待两个技术?
    //不矛盾
    //

    /**
     * 关于Class类的理解
     * 1、类的加载过程
     * 程序经过javac. exe命令以后，会生成一个或多个字节码文件(.class结尾),接着我们使用
     * java. exe命令对某个字节码文件进行解释运行。相当于将某个字节码文件加载到内存的过程就称之为类的加载。
     * 再加到内存中的类，我们就称之为运行时类，此运行时类，就作为一个Class的一个实例
     * <p>
     * 2.换句话说，class的实例就对应着一个运行时类,而且只会创建一个
     */
    //获取class类实例的方式：
    @Test
    public void test3() throws ClassNotFoundException {
        //方式一:调用运行时类的属性:.class
        Class clazz1 = Person.class;
        System.out.println(clazz1);
        //方式二：通过运行时类的对象,调用getclass方法
        Person p1 = new Person();
        Class clazz2 = p1.getClass();
        System.out.println(clazz2);
        //方法三：调用class的静态方法(用的最多)
        Class clazz3 = Class.forName("Day8.Person");

        System.out.println(clazz3);

        System.out.println("java.lang.String");
        //获取的都是一个运行时类
        System.out.println(clazz1 == clazz2);
        System.out.println(clazz1 == clazz3);
    }

    //CLass实例可以是哪些结构的说明:
    @Test
    public void test4() {
        Class c1 = Object.class;
        Class c2 = Comparable.class;
        Class c3 = String[].class;
        Class c4 = int[][].class;
        Class c5 = ElementType.class;
        Class c6 = Override.class;
        Class c7 = int.class;
        Class c8 = void.class;
        Class c9 = Class.class;
        int[] a = new int[10];
        int[] b = new int[100];
        Class c10 = a.getClass();
        Class c11 = b.getClass();
        //只要元素类型与维度一样，就是同一个CLass,不用考虑类里面的细节
        System.out.println(c10 == c11);
    }
}
