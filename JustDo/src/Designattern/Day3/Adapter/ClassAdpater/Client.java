package Designattern.Day3.Adapter.ClassAdpater;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/19 8:00
 * @description： 客户端 类适配器
 * 用其他类的方法有三种方法：代码块，构造器，以及方法
 */
public class Client {
    public Phone phone;

    {
        getPone(new Phone());
    }


    public Client() {
        getPone(new Phone());
    }

    public void getPone(Phone phone){
        this.phone=phone;
        this.phone.ChongDian();
    }
    public static void main(String[] args) {
        new Client();
    }
}
