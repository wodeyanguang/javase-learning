package Designattern.Day3.fiyweight;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/21 9:32
 * @description：
 */
public class ConcrateWebSite extends WebSite{
    private String type="";

    public ConcrateWebSite(String type) {
        this.type = type;
    }

    @Override
    public void use() {
        System.out.println("正在使用的类型为："+type);
    }
}
