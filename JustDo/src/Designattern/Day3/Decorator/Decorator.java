package Designattern.Day3.Decorator;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/20 10:14
 * @description：
 */
public class Decorator extends Drink{
    private Drink drink;//组合关系

    public Decorator(Drink drink) {
        this.drink = drink;
    }

    @Override
    public int getcose() {
        return getCost()+this.drink.getcose();
    }

    @Override
    public String getdscc() {
        return getDsc()+this.drink.getDsc();
    }
}
