package Designattern.Day3.Decorator;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/20 9:52
 * @description：
 */
public class ADrink extends Coffee{
    public ADrink() {
        setCost(1);
        setDsc("这是ACOffee");
    }
}
