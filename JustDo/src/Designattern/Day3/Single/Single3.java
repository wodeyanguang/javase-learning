package Designattern.Day3.Single;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/18 8:38
 * @description： 双重检查
 * 解决了线程安全问题和懒加载问题
 * 推荐使用
 */
public class Single3 {
    public static void main(String[] args) {
        GoodSingle instance = GoodSingle.getInstance();
        System.out.println(instance.hashCode());
    }
}
class GoodSingle{
    private static volatile GoodSingle goodSingle;

    private GoodSingle() {
    }

    public static GoodSingle getInstance(){
        if (goodSingle == null){
            synchronized (GoodSingle.class){
                if (goodSingle == null){//这个地方如果不加这个双重判断语句的话
                    //会有好几个线程同时进入第一个if判断的条件里面来，然后一个一个的new对象，所以没有意义
                    //在这里我们用双重判断语句，当第二个线程进入同步代码块的时候，就不会再继续new对象了
                    goodSingle = new GoodSingle();
                }
            }
        }
        return goodSingle;
    }
}
