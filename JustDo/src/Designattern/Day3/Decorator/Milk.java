package Designattern.Day3.Decorator;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/20 10:18
 * @description：
 */
public class Milk extends Decorator{
    public Milk(Drink drink) {
        super(drink);
        setCost(4);
        setDsc("这是一杯牛奶");
    }
}
