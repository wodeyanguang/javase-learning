package Day4;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/29 7:57
 * @description：String、StringBuffer、 StringBuilder三 者的异同?
 * String:不可变的字符序列;
 * StringBuffer:可变的字符序列;线程安全的，效率低;
 * StringBuilder:可变的字符序|; jdk5. e新增的，线程不安全的，效率高;
 * StringBuffer append(xxx): 提供了很多的append()方法，用于进行字符串拼接
 * StringBuffer delete(int start, int end):删除指定位置的内容
 * StringBuffer replace(int start, int end, String str):把[start, end)位置替换为str
 * StringBuffer insert(int offset, xxx): 在指定位置插入xxx
 * StringBuffer reverse() :把当前字符序列逆转
 * public int indexOf(String str)返回索引
 * public String substring(int start, int end)返回索引,有返回值
 * public int Length()
 * public char charAt(int n )
 * public void setCharAt(int n , char ch)
 */
public class StringBufferBuilderTest {
    public static void main(String[] args) {
        StringBuffer sb1=new StringBuffer("abc");
        sb1.setCharAt(0,'A');
        System.out.println(sb1);

        StringBuffer s1=new StringBuffer("abd");
        s1.append(1);
        s1.append('2');
        System.out.println(s1);
        s1.delete(2,4);
        System.out.println(s1);
        s1.insert(1,'c');
        System.out.println(s1);

        s1.reverse();
        System.out.println(s1);

        s1.setCharAt(3,'A');
        System.out.println(s1);

        s1.replace(2,3,"wode");
        System.out.println(s1);

        String s2 = s1.substring(0, 3);
        System.out.println(s2);
    }
}
