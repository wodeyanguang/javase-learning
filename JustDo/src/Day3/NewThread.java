package Day3;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/28 14:06
 * @description：Callable接口创建多线程,有返回值的额线程
 * Future接口
 */
class NumTread implements Callable{//创建一个实现类
    //执行call方法,可以有返回值
    @Override
    public Object call() throws Exception {
        int sum=0;
        for (int i = 1; i <=100 ; i++) {
            if (i%2==0){
                System.out.println(i);
                sum+=i;
            }

        }
        return sum;
    }
}
public class NewThread {
    public static void main(String[] args) {
        //创建接口实现类的对象
        NumTread numTread=new NumTread();
        //将接口传递到构造器中FutrueTask
        FutureTask futureTask = new FutureTask(numTread);
        //跑起来
        new Thread(futureTask).start();
        //get方法的返回值就是Callable重写的call方法的返回值
        try {
            Object sum = futureTask.get();//获取返回值
            System.out.println("总和为:"+sum);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
