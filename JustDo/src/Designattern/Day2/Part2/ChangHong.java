package Designattern.Day2.Part2;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/17 15:53
 * @description： 实现接口之间的依赖关系，相互调用方法
 */
public class ChangHong implements PlayTV{

    @Override
    public void playTv() {
        System.out.println("长虹电视机正在开机...");
    }

    public static void main(String[] args) {
        //方式一：
//        Open open = new Open();
//        open.setTv(new ChangHong());
//        open.play();
        //方式二：
        Open open = new Open(new ChangHong());
        open.play();


    }
}
