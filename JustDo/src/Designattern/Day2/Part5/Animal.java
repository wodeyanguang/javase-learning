package Designattern.Day2.Part5;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/17 17:21
 * @description：
 */
public class Animal {
    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public void run(){
        System.out.println(this.name+"正在跑步");
    }
}
