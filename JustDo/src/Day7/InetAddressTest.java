package Day7;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/3 19:16
 * @description：
 * /**
 * *一、网络编程中有两个主要的问题:
 * * 1. 如何准确地定位网络上一台或多台主机:定位主机上的特定的应用
 * * 2.找到主机后如何可靠高效地进行数据传输
 * *二、网络编程中的两个要素:
 * * 1.对应问题一: IP和端口号
 * * 2.对应问题二:提供网络通信协议: TCP/IP参考模型(应用层、传输层、网络层、物理+数据链
 * *三、通信要素一: IP和端口号
 * *
 * * 1. IP:唯-的标识Internet上的计算机 (通信实体)
 * * 2.在Java 中使用InetAddress类代表IP
 *3.IP分类: IPv4 和IPv6 ;
 * 万维网和局域网
 *本地回路地址127.0.0.1，对应一个localhost本机
 */
public class InetAddressTest {
    public static void main(String[] args) {
        try {
            InetAddress inet1 = InetAddress.getByName("192.168.10.14");//获取名字
            System.out.println(inet1);
            InetAddress inet2=InetAddress.getByName("www.baidu.com");//域名
            System.out.println(inet2);
            InetAddress inet3=InetAddress.getLocalHost();//获取本机的IP地址
            System.out.println(inet3);
        }catch (UnknownHostException e){
            e.printStackTrace();
        }
    }
}
