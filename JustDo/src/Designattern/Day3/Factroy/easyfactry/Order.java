package Designattern.Day3.Factroy.easyfactry;

import java.util.Scanner;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/18 9:53
 * @description： 简单工厂就是说，尽量的话把生产什么定西都封装到一个类里面，
 * 要新增一个的时候就比较简单
 * 这里的话，就只用增加一个类然后修改一下factory类就可以了，其他的都不用变
 * 包括工厂方法模式也是一样的，就是把制造工厂返回一个Pizz这个类抽象化，然后再写子类分别继承他，然后重写抽象方法
 * 中心思想就是如果类要创建的多了就统一的继承一个抽象类（或者一个一般的类）
 * 工厂帮你造pizz，这一个类的代码完全不用改
 */
public class Order {
    public static void main(String[] args) {
        new Order();
    }
    public Factory factory;

    public Order() {
        setFactory();
    }

    public void setFactory(){
        while (true) {
            String type = Type();
            Pizz factoryPizz = factory.createPizz(type);
            if (factoryPizz!=null) {
                factoryPizz.process();
                factoryPizz.eat();
            }else {
                break;
            }
        }
    }

    public String Type(){
        System.out.println("请输入披萨的种类：");
        Scanner good= new Scanner(System.in);
        String  pizztype= good.next();
        return pizztype;
    }
}


