package Day9;

import Day8.Person;
import org.junit.Test;

import java.util.*;
import java.util.stream.Stream;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/5 18:10
 * @description： 测试stream中间操作
 */
public class SteamAPITest1 {
    //1-筛选与切片
    @Test
    public void test1() {
        List<Integer> list = new LinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(5);
//        filter(过滤)(Predicate p)一接收Lambda，从流 中排除某些元素。
        Stream<Integer> stream = list.stream();
        stream.filter(a -> a > 2).forEach(System.out::print);//过滤器,终止操作就是遍历
        System.out.println();
//        limit(n)-截断流，使其元素不超过给定数量。
        list.stream().limit(3).forEach(System.out::print);
        System.out.println();
//        skip(n) -跳过元素，返回一个扔掉了前n个元素的流。若流中元素不足n个，则返回一
        list.stream().skip(3).forEach(System.out::print);
        System.out.println();
//        distinct()-筛选，通过流所生成元素的hashCode()和equals()去除重复元素
        list.stream().distinct().forEach(System.out::print);
    }

    @Test
    //映射
    public void test2() {
//        map(Function f)- 一接收一个函数作为参数， 将元素转换成其他形式或提取信息，该函数会被应用到每个元素上，并将其映射成一个新的元素
        List<String> list = Arrays.asList("aa", "bb", "cc");
        list.stream().map(str -> str.toUpperCase()).forEach(System.out::print);
        System.out.println();
//        练习1:获取员工姓名长度大于3的员工的姓名。
        Person p1 = new Person("闫广", 20);
        Person p2 = new Person("刘程浩", 19);
        Person[] ps = {p1, p2};
        List<Person> personList = Arrays.asList(ps);
        Stream<String> nameStream = personList.stream().map(e -> e.getName());//Lambda表达式用作一个函数的时候，可以换成方法引用，这里换成Person：：getname一样是可以的
        nameStream.filter(name -> name.length() > 2).forEach(System.out::print);
        System.out.println();
        //练习2：
        //相当于集合里面有一个集合{1，2，{3,4}}
        Stream<Stream<Character>> streamStream = list.stream().map(SteamAPITest1::fromStringToStream);
//        所以需要两次遍历
        streamStream.forEach(s->{
            s.forEach(System.out::print);
        });
        System.out.println();
//        flatMap(Function f)-接收一 个函数作为参数， 将流中的每个值都换成另一一个流， 然后把所有流连接成一个流
        Stream<Character> characterStream = list.stream().flatMap(SteamAPITest1::fromStringToStream);
        characterStream.forEach(System.out::print);//这样简单多了,可以拆集合中的集合
    }

    //将字符串中的多个字符构成的集合转换为对应的Stream的实例
    public static Stream<Character> fromStringToStream(String str) {
        ArrayList<Character> /*char类型的意思*/ list = new ArrayList<>();
        for (Character c : str.toCharArray()) {
            list.add(c);
        }
        return list.stream();
    }
    //排序
    @Test
    public void test3(){
//        sorted()-自然排序
        List<Integer> list = Arrays.asList(12, -3, 23, 54, 7, 86);
        list.stream().sorted().forEach(System.out::println);

//        sorted( Comparator com,)-定制排序
        Person p1 = new Person("闫广", 20);
        Person p2 = new Person("刘程浩", 19);
        Person[] ps = {p1, p2};
        List<Person> personList = Arrays.asList(ps);
        personList.stream().sorted((e1,e2)->Integer.compare(e1.getAge(),e2.getAge())).forEach(System.out::print);
        System.out.println();
        personList.stream().sorted(new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return Integer.compare(o1.getAge(),o2.getAge());
            }
    }).forEach(System.out::print);
    }

}
