package Designattern.Day3.Factroy.GoodFactory;

import Designattern.Day3.Factroy.GoodFactory.PizzBean.Pizzs;

public interface AbFactory {
   public Pizzs createPizzs(String type);
}
