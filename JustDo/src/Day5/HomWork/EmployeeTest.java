package Day5.HomWork;

import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/30 20:30
 * @description：由于这里是不可重复的所以用Set方法,然后针对对象的属性进行排序，所以用TreeSet
 * comparator比较器在第二块使用，compare比较器在第一块使用
 * 一个定制专属比较，一个是大众的简单比较
 */
public class EmployeeTest {

    public static void main(String[] args) {
        //自然排序
        TreeSet set=new TreeSet();
        Emloyee e1=new Emloyee("zhangsan",18,new MyDate(1960,1,9));
        Emloyee e2=new Emloyee("wangermazi",3,new MyDate(1830,4,19));
        Emloyee e3=new Emloyee("lisi",64,new MyDate(2000,3,8));
        Emloyee e4=new Emloyee("wangwu",57,new MyDate(1920,18,2));

        set.add(e1);
        set.add(e2);
        set.add(e3);
        set.add(e4);

        Iterator iterator = set.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }


        TreeSet set1=new TreeSet(new Comparator() {//指定专属的比较方式,直接括号里面new
            @Override
            public int compare(Object o1, Object o2) {
                if (o1 instanceof Emloyee && o2 instanceof Emloyee){
                    Emloyee e1=(Emloyee)o1;
                    Emloyee e2=(Emloyee)o2;

                    MyDate b1 = e1.getBirthday();
                    MyDate b2 = e2.getBirthday();

                    int minusYear = b1.getYear()-b2.getYear();//这个地方可以改排列的顺序
                    if (minusYear!=0){//根据大于0小于0,自动判断大小
                        return minusYear;
                    }
                    int minusMonth = b1. getMonth() - b2. getMonth( );
                    if(minusMonth != 0){
                        return minusMonth ;
                    }
                    return b1.getDay() - b2. getDay();
                }
                throw new RuntimeException("传输数据类型不一致");
            }
        });


        Emloyee e5=new Emloyee("zhangsan",18,new MyDate(1960,1,9));
        Emloyee e6=new Emloyee("wangermazi",3,new MyDate(1830,4,19));
        Emloyee e7=new Emloyee("lisi",64,new MyDate(2000,3,8));
        Emloyee e8=new Emloyee("wangwu",57,new MyDate(1920,18,2));

        set1.add(e5);
        set1.add(e6);
        set1.add(e7);
        set1.add(e8);

        Iterator iterator1 = set1.iterator();
        while (iterator1.hasNext()){
            System.out.println(iterator1.next());
        }
    }
}
