package Designattern.Day3.fiyweight;

import java.util.HashMap;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/21 9:33
 * @description： 享元模式，就是共享对象
 * 这里主要就是利用了HashMap这个集合,就是不要造太多重复的对象
 * 用一个集合装起来，然后方便进行查看
 */
public class WebSiteFactory {

    HashMap<String,ConcrateWebSite> pool = new HashMap<>();

    public WebSite getConcrateWebSite(String type){
        if (!pool.containsKey(type)){
            pool.put(type,new ConcrateWebSite(type));
        }
        return (WebSite)pool.get(type);
    }
    public int getcount(){
        int size = pool.size();
        return size;
    }

    public static void main(String[] args) {
        WebSiteFactory factory = new WebSiteFactory();
        WebSite site = factory.getConcrateWebSite("网站");
        site.use();
        WebSite site1 = factory.getConcrateWebSite("博客");
        site1.use();
        WebSite site2 = factory.getConcrateWebSite("博客");
        site2.use();
        int getcount = factory.getcount();
        System.out.println(getcount);
    }
}
