package JDBC_2.transaction;

import JDBC_1.util.JDBCUtils;
import org.junit.jupiter.api.Test;

import java.sql.Connection;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/15 8:32
 * @description：
 */
public class ConnectionTest {
    @Test
    public void testgetConnection() throws Exception {
        Connection connection = JDBCUtils.getConnection();
        System.out.println(connection);
    }
}
