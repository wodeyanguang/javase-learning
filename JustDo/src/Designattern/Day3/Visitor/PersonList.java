package Designattern.Day3.Visitor;

import java.util.LinkedList;
import java.util.List;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/21 15:12
 * @description： 访问者模式
 * 主要就是还是面向接口跟抽象类编程，然后重复的行为划分一下
 */
public class PersonList {
    List<Person> personList = new LinkedList<>();

    public void addList(Person p){
        personList.add(p);
    }

    public void seeAll(Action action){
        for (Person p :personList) {
            p.accpet(action);
        }

    }

    public static void main(String[] args) {
        PersonList personList = new PersonList();

        System.out.println("-----------集体投票---------------");
        personList.addList(new Man());
        personList.addList(new Woman());
        personList.addList(new Man());
        personList.seeAll(new Win());

        System.out.println("------------个人投票------------");
        Man man = new Man();
        man.accpet(new Fail());
    }
}
