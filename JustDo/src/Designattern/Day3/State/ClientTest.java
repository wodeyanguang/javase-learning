package Designattern.Day3.State;

/**
 * @author Administrator
 *  状态模式
 *  其实就是定义一个状态的抽象类，然后顶一个context就是这里的抽奖机
 *  里面存放当前的状态，然后再定义子类来写出不同的状态，也可以放入到一个枚举类当中
 *  最后进行当前状态的改变
 */
public class ClientTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// 初始化一下，当前的一个奖品数量
        RaffleActivity activity = new RaffleActivity(1);

        // 进行抽奖多少次
        for (int i = 0; i < 30; i++) {
            System.out.println("--------第" + (i + 1) + "次抽奖----------");
            //进行抽奖判断
            activity.debuctMoney();
            //发送奖品
            activity.raffle();
        }
	}

}
