package Day9;

import Day8.Person;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/5 8:35
 * @description： 调用运行时类中指定的结构:属性、方法、构造器
 */
public class ReflectionTest {
    @Test
    public void test1() throws Exception {
        Class<Person> clazz = Person.class;//创建一个class对象

        Person person = clazz.newInstance();//创建一个person对象
        //一般不用这个用getdeclareField
        Field age = clazz.getField("age");//利用class获取属性，但是只能是public的属性

        age.set(person,88);

        int page= (int) age.get(person);

        System.out.println(page);

        Field birthday = clazz.getDeclaredField("birthday");

        birthday.setAccessible(true);//这个是核心,必须要修改权限，才能改

        birthday.set(person,2000);

        birthday.get(person);

        System.out.println(birthday);
    }

    @Test
    public void test2() throws Exception {
        Class<Person> clazz = Person.class;

        Person person = clazz.newInstance();
        //获取方法
        Method showNation = clazz.getDeclaredMethod("showNation", String.class);
        /*
        用invoke()调用方法,invoke()方法的返回值就是调用方法的返回值
         */
        showNation.setAccessible(true);

        Object china = showNation.invoke(person, "china");//可以获取返回值

        System.out.println(china);

        Method happy = clazz.getDeclaredMethod("happy");//用declare可以调用静态方法

        happy.setAccessible(true);

        Object invoke = happy.invoke(person);//如果调用的方法没有返回值就返回null

        System.out.println(invoke);
    }
}
