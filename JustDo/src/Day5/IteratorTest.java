package Day5;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/30 9:06
 * @description：集合元素的遍历 Iterator对象称为迭代器(设计模式的一种)，主要用于遍历Collection集合中的元素。
 * hasNext()方法好用
 */
public class IteratorTest {
    @Test
    public void test2() {

        {
            Collection coll = new ArrayList();
            //add(0bject e)装入的是类
            coll.add("aa");
            coll.add("bb");
            coll.add(123);//自动装箱

            Iterator iterator = coll.iterator();

            System.out.println(iterator.next());

            while (iterator.hasNext()) {
                //next():a指针下移2将下移以后集合位置上的元素返回
                Object object = iterator.next();//相当于指针
                if ("aa".equals(object)) {
                    iterator.remove();//删除
                }
                System.out.println(iterator.next());
            }
            iterator = coll.iterator();
            while (iterator.hasNext()) {
                System.out.println(iterator.next());
            }
        }
    }
}
