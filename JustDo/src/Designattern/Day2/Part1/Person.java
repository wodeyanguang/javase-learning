package Designattern.Day2.Part1;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/17 15:34
 * @description： 应用类
 */
public class Person {
    public void receive(recever r){
        System.out.println(r.getInfo());
    }
//这里的思想就是实现类需要有一个使用类来调用这些实现类里面的方法
    public static void main(String[] args) {
        Person person = new Person();
        person.receive(new QQ());
        person.receive(new WeiXin());
    }
}
