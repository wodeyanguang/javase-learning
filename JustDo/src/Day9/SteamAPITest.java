package Day9;

import Day8.Person;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/5 17:28
 * @description： 1. Stream关注的是对数据的运算，与CPU打交道
 * 集合关注的是数据的存储，与内存打交道
 * <p>
 * ①Stream自己不会存储元素。
 * ②Stream不会改变源对象。相反，他们会返回-一个持有结果的新Stream。
 * ③Stream操作是延迟执行的。这意味着他们会等到需要结果的时候才执行
 * <p>
 * 3.Stream执行流程
 * ①Stream类的实例化
 * ②一系列的中间操作(过滤、映射、..
 * ③终止操作
 * <p>
 * 4.说明:
 * 4.1 -个中间操作链，对数据源的数据进行处理
 * 4.2 -旦执行终止操作，就执行中间操作链，并产生结果。之后，不会再被使用
 */
public class SteamAPITest {
    @Test
    public void test1() {
        //创建Stream方式一-:通过集合
        List<Integer> list = new LinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
//        default Stream<E> stream() :返回一个顺序流
        Stream<Integer> stream = list.stream();
//        default Stream<E> parallelStream() :返回一个并行流
        Stream<Integer> parallelStream = list.parallelStream();
    }

    @Test
    public void test2() {
        //创建stream方式二:通过数组
        int[] arr={1,2,3,4,5};
        //调用Arrays类的static <T> Stream<T> stream(T[] array): 返回一个流
        IntStream stream = Arrays.stream(arr);

        Person p1 =new Person("闫广",20);
        Person p2 =new Person("流程浩",19);
        Person[] ps={p1,p2};
        Stream<Person> stream1 = Arrays.stream(ps);
    }
    @Test
    public void test3(){
        //创建Stream方式三:通过Stream的of()
        Stream<Integer> stream = Stream.of(1, 2, 3, 4, 5, 6);

    }

}
