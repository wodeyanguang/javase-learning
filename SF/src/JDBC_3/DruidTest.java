package JDBC_3;

import com.alibaba.druid.pool.DruidDataSourceFactory;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Properties;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/16 9:26
 * @description：    Druid数据库连接池
 */
public class DruidTest {
    //直接通过配置文件
    @Test
    public void testConnetion() throws Exception {
        //声明一个配置文件
        Properties pros = new Properties();
        //创建一个输入流
        InputStream rs = ClassLoader.getSystemClassLoader().getResourceAsStream("druid.properties");
        //加载配置文件
        pros.load(rs);
        //创建一个druid数据库连接池
        DataSource source = DruidDataSourceFactory.createDataSource(pros);
        //创造连接
        Connection connection = source.getConnection();
        System.out.println(connection);
    }
}
