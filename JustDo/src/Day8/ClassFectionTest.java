package Day8;

import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/4 20:15
 * @description： 反射来获取属性
 */
public class ClassFectionTest {
    @Test
    public void test2() {
        Class clazz = Person.class;
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field f : declaredFields) {
        //1.权限修饰符
            int modifier = f.getModifiers();
            System.out.print(Modifier.toString(modifier) + "\t");
        //2.数据类型
            Class type = f.getType();
            System.out.println(type + "\t");
        //3.变量名
            String fname = f.getName();
            System.out.println(fname);
        }
        Class clazz1 = Person.class;
//
        Method[ ] methods = clazz1 . getMethods( );
        for(Method m : methods){
            System. out . println(m);
        }
    }

}
