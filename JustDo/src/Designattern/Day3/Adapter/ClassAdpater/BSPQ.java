package Designattern.Day3.Adapter.ClassAdpater;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/19 7:50
 * @description： 被适配器
 */
public class BSPQ {
    public int Output220v(){
        int outputv=220;
        System.out.println("输出电压为"+outputv);
        return outputv;
    }
}
