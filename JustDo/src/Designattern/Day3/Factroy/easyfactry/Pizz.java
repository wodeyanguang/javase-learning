package Designattern.Day3.Factroy.easyfactry;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/18 9:39
 * @description：
 */
public abstract class Pizz {
    protected String name;

    public abstract void process();

    public void eat(){
        System.out.println("正在吃"+name);
    }

    public void setName(String name){
        this.name=name;
    }
}
