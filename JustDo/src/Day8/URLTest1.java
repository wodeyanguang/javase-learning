package Day8;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/4 8:59
 * @description：
 */
public class URLTest1 {
    public static void main(String[] args) throws IOException {
        //写一个网址
        URL url=new URL("http://localhost :8080/examples/beauty. jpg?username=Tom");
       //获取网址连接
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();//拿到一个连接
        //正式连接
        urlConnection.connect();
        //将网络上的数据输入
        InputStream is = urlConnection.getInputStream();
        //输出到本地
        FileOutputStream fos=new FileOutputStream("7.png");

        byte[] buffer = new byte[1024];
        int len;
        while((len = is. read(buffer)) != -1){
            fos.write(buffer, 0,len);
        }

        //关闭资源

        is.close();
        fos.close();
        urlConnection.disconnect();


    }
}
