package Designattern.Day2.Part4;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/17 17:05
 * @description： 这里的话就是说尽量完成好的代码就不要修改
 * 如果要进行功能的一个添加的话，就在原有的基础上进行代码的增加
 * 所以这里创造了一个抽象类的抽象方法，然后子类就有各自的抽象方法
 * 的实际方法，然后这样就很方便的进行代码的增加了
 * 抽象类：你只能去使用它，你不能实例化
 * 抽象方法：你必须重写，有抽象方法的类，必须是抽象类
 */
public class Draw {
    public void Drawthis(Shape shapes){
        shapes.draw();
    }

    public static void main(String[] args) {
        Yuan yuan = new Yuan();
        yuan.draw();
        ZFX zfx = new ZFX();
        zfx.draw();
    }
}
