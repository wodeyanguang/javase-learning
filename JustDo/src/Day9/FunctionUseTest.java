package Day9;

import org.junit.Test;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/5 15:18
 * @description： 方法引用的使用
 *1.使用情境:当要传递给Lambda体的操作， 已经有实现的方法了，可以使用方法引用!
 *
 * 2.方法引用，本质_上就是Lambda表达式，而Lambda 表达式作为函数式接口的实例。所以
 * 方法引用，也是函数式接口的实例。
 *
 * 3.
 * 使用格式:类(或对象)::方法名
 *
 * 4.
 * 对象::非静态方法
 * 类::静态方法
 * 类::非静态方法
 *
 * 5.方法引用使用的要求:要求接口中的抽象方法的形参列表和返回值类型与方法引用的方法的
 * 形参列表和返回值类型相同!
 */
public class FunctionUseTest {
    //情况一:对象::实例方法
    //Consumer中的void accept(T t)可以由下面的方法替换，因为类型是一致的
    //PrintStream中的void println(T t)


    @Test
    public void test1(){
        Consumer<String> con1 = str -> System.out.println(str);

        con1.accept("闫广");

        System.out.println(")))))))))))))))))方法引用如下)))))))))))))))");

        PrintStream ps= System.out;
        Consumer<String> con2 = ps :: println;
        con2.accept("重写 闫广");//accept方法由print方法重写了

    }
    @Test
    public  void test2(){
//        Function可以找到对应类中的函数的（形参属性，返回值）
        Function<Integer,String[]> f1 = length -> new String[length];

        String[] apply = f1.apply(5);
        System.out.println(Arrays.toString(apply));
    }
}
