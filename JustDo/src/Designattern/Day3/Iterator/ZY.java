package Designattern.Day3.Iterator;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/21 20:20
 * @description： 专业
 */
public class ZY {
    private String name;
    private String info;

    public ZY(String name, String info) {
        this.name = name;
        this.info = info;
    }

    public ZY() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
