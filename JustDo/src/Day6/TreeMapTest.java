package Day6;

import org.junit.Test;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/2 11:18
 * @description：
 */
public class TreeMapTest {

    //向TreeMap中添加key-value，要求key必须是由同-一个类创建的对象
    // 因为要按照key进行排序:自然排序、定制排序

      @Test//自然排序
    public void test1(){
          TreeMap map=new TreeMap();

          map.put("Tom", 34234);
          map.put("Sam", 254);
          map.put("Bob", 14676);

          Set keySet = map.keySet();
          Iterator iterator = keySet.iterator();
          while( iterator.hasNext()) {
              Object key = iterator.next();
              Object value = map.get(key);
              System.out.println(key + "=====" + value);
          }
      }
//    @Test//定制排序
//    public void test2(){
//        TreeMap map=new TreeMap(new Comparator() {
//            @Override       按照年龄进行排序
//            public int compare(Object o1, Object o2) {
//                if(o1 instanceof User && o2 instanceof User){
//                    User u1 = (User)o1;
//                    User u2 = (User)o2;
//                    return Integer . compare(u1. getAge(), u2. getAge());
//                    throw new RuntimeException( "输入的类型不匹配! ");
//
//                }
//        });
//
//        map.put("Tom", 34234);
//        map.put("Sam", 254);
//        map.put("Bob", 14676);
//
//        Set keySet = map.keySet();
//        Iterator iterator = keySet.iterator();
//        while( iterator.hasNext()) {
//            Object key = iterator.next();
//            Object value = map.get(key);
//            System.out.println(key + "=====" + value);
//        }
//    }

}
