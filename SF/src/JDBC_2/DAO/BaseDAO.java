package JDBC_2.DAO;

import JDBC_1.util.JDBCUtils;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/15 12:02
 * @description： 封装了对于数据表通用的增删改查操作
 * 不能用于实例化，就是一些方法
 */
public abstract class BaseDAO {
    //通用的增删改查操作---version 2.0
    public int update(Connection connection, String sql, Object... args) {
        PreparedStatement ps = null;
        try {
            //1.预写sql
            ps = connection.prepareStatement(sql);
            //2.填充占位符
            for (int i = 0; i < args.length; i++) {
                ps.setObject(i + 1, args[i]);//小心参数声明错误
            }
            //3.执行语句
            return ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //4.关闭
            JDBCUtils.closeResourece(null, ps);
        }
        return 0;
    }

    //统一的查询操作(vserion 2.0,考虑了事务进来)
    public <T> T getInstance(Connection connection, Class<T> clazz, String sql, Object... args) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                ps.setObject(i + 1, args[i]);
            }
            //执行，获得结果集
            rs = ps.executeQuery();
            //获取结果集的元数据
            ResultSetMetaData rsmd = rs.getMetaData();
            //获取字段据个数
            int columnCount = rsmd.getColumnCount();
            if (rs.next()) {
                //获取一个类的对象
                T t = clazz.newInstance();
                for (int i = 0; i < columnCount; i++) {
                    //获取每一个列的列值:通过resultset
                    Object columnValue = rs.getObject(i + 1);
                    //获取列的别名：getcolumnlabel（）推荐使用
                    String columnName = rsmd.getColumnLabel(i + 1);
                    //通过反射将对象指定属性名的属性赋值
                    Field field = clazz.getDeclaredField(columnName);
                    field.setAccessible(true);
                    field.set(t, columnValue);
                }
                return t;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResourece(null, ps, rs);
        }
        return null;
    }

    //查询多行version 2.0
    public <T> List<T> getForList(Connection connection,Class<T> clazz, String sql, Object... args) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                ps.setObject(i + 1, args[i]);
            }
            //执行，获得结果集
            rs = ps.executeQuery();
            //获取结果集的元数据
            ResultSetMetaData rsmd = rs.getMetaData();
            //获取字段据个数
            int columnCount = rsmd.getColumnCount();
            //创建集合对象
            ArrayList<T> list = new ArrayList<>();
            while (rs.next()) {
                //获取一个类的对象
                T t = clazz.newInstance();
                //给T对象复制的过程
                for (int i = 0; i < columnCount; i++) {
                    //获取每一个列的列值:通过resultset
                    Object columnValue = rs.getObject(i + 1);
                    //获取列的别名：getcolumnlabel（）推荐使用
                    String columnName = rsmd.getColumnLabel(i + 1);
                    //通过反射将对象指定属性名的属性赋值
                    Field field = clazz.getDeclaredField(columnName);
                    field.setAccessible(true);
                    field.set(t, columnValue);
                }
                list.add(t);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResourece(null, ps, rs);
        }
        return null;
    }
    //用于查询特殊值的方法
    public <E> E getValue(Connection connection,String sql,Object ...args){
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                ps.setObject(i+1,args[i]);
            }
            rs = ps.executeQuery();

            if (rs.next()){
                return (E) rs.getObject(1);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            JDBCUtils.closeResourece(null,ps,rs);
        }
        return null;
    }
}
