package Designattern.Day3.Iterator;

import java.util.Iterator;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/21 20:22
 * @description：
 */
public class ComputerIterator implements Iterator {
     ZY[] zies ;
     int index=0;

    public ComputerIterator(ZY[] zies) {
        this.zies = zies;
    }

    @Override
    public boolean hasNext() {
        if (index>zies.length||zies[index]==null){
            return false;
        }else {
            return true;
        }
    }

    @Override
    public Object next() {
        ZY zy = zies[index];
        index++;
        return zy;
    }

    @Override
    public void remove() {

    }
}
