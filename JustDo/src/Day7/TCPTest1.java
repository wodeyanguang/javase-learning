package Day7;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/3 19:50
 * @description： 实现TCP的网络编程
 * 例子1:客户端发送信息给服务端，服务端將数据显示在控制台上
 *
 * 客户端与服务器之间的首先要做的事情就是客户端创建一个端口号和IP地址
 * 服务器端只需要创建端口号就可以了，只用负责接收
 */
public class TCPTest1 {
    //客户端
    @Test
    public void client() {
        //1.创建socket对象，指定ip和端口号
        Socket socket = null;//port是端口号,socket是端口号和IP的合体
        //2.获取一个输出流，输出数据
        OutputStream os = null;
        //3.写出数据操作
        try {
            InetAddress inet = InetAddress.getByName("127.0.0.1");
            socket = new Socket(inet, 8899);

            os = socket.getOutputStream();//获取输出流
            os.write("你好，我是客户端".getBytes());//输出字节
        } catch (IOException e) {
            e.printStackTrace();
            //4.资源的关闭
        } finally {

            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    //服务端（需要先启动服务器）
    @Test
    public void server() {
        ServerSocket ss = null;
        Socket socket = null;//接收socket
        InputStream is = null;//获取输入流
        ByteArrayOutputStream baos = null;
        try {
            //1.创建服务器端的socket，只用指明自己的端口号
            ss = new ServerSocket(8899);
            //2.调用方法来接收客户端的socket
            socket = ss.accept();
            //3.声明输入流
            is = socket.getInputStream();
//        不建议这样写，容易有乱码
//        byte[] buffer=new byte[20];
//        int len;
//        while ((len=is.read(buffer))!=-1){
//            String str=new String(buffer,0,len);
//            System.out.println(str);
//        }
            //4.读取数据
            baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[5];
            int len;
            while ((len = is.read(buffer)) != -1) {
                baos.write(buffer, 0, len);
            }
            System.out.println(baos.toString());

            System.out.println("收到了来自于"+socket.getInetAddress().getHostAddress()+"的客户端连接");
        } catch (IOException e) {
            e.printStackTrace();
            //5.关闭资源
        } finally {
            if (baos != null) {
                try {
                    baos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (ss != null) {
                try {
                    ss.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

    }
}
