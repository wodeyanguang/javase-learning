package Designattern.Day3.Factroy.GoodFactory.PizzBean;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/18 14:53
 * @description：
 */
public class LDUSApizz extends Pizzs{
    @Override
    public void Prepared() {
        System.out.println("正在加工LD美国披萨");
    }
}
