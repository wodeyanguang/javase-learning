package Day7;

import org.junit.Test;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/3 20:51
 * @description：
 * 实现TCP的网络编程
 * 例题2:客户端发送文件给服务端，服务端将文件保存在本地。
 *客户端：
 * 首先需要造一个输入流，将本地的文件输入java，然后创建一个输出流，将文件输出出去
 *
 * 服务器端：
 * 首先要进行socket的一个接受，然后造一个输入流，将客户端发来的数据进行一个接受，然后造一个输出流，将数据从java中进行输出到电脑指定的路径下
 */
public class TCPTest2 {

    @Test//客户端
    public void client() throws IOException {
        //1.造一个socket
        Socket socket=new Socket(InetAddress.getByName("127.0.0.1"),9090);//造一个socket
        //2.获取一个输出流
        OutputStream os = socket.getOutputStream();//造一个输出流
        //3.获取一个输入流，从哪个文件里面去读数据
        FileInputStream fis=new FileInputStream(new File("4.png"));//将图片写入,利用已有的文件
       //4.读数据，写数据
        byte[] buffer=new byte[1024];
        int len;
        while ((len = fis.read(buffer))!=-1){
            os.write(buffer,0,len);//将输入传输出去
        }
        //5.资源关闭
        fis.close();
        os.close();
        socket.close();
    }
    @Test//服务器
    public void server() throws IOException {
        //1.造一个socket
        ServerSocket ss = new ServerSocket(9090);
        //2.接收一下客户端的socket
        Socket socket = ss.accept();
        //3.获取客户端的输入流
        InputStream is = socket.getInputStream();
        //4.将数据输入到本地，建立一个本地的输出流
        FileOutputStream fos=new FileOutputStream(new File("6.png"));
        //5.读写过程
        byte[] buffer=new  byte[1024];
        int len;
        while ((len=is.read(buffer))!=-1){
            fos.write(buffer,0,len);
        }
        //6.资源关闭
        fos.close();
        is.close();
        socket.close();
        ss.close();
    }
}
