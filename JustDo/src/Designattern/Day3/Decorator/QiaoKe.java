package Designattern.Day3.Decorator;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/20 10:18
 * @description：
 */
public class QiaoKe extends Decorator{
    public QiaoKe(Drink drink) {
        super(drink);
        setCost(5);
        setDsc("这是一块儿巧克力");
    }
}
