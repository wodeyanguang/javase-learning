package Designattern.Day3.Visitor;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/21 15:08
 * @description：
 */
public class Fail extends  Action{

    @Override
    public void getManinfo(Man man) {
        System.out.println("男觉得你不行！");
    }

    @Override
    public void getWomaninfo(Woman woman) {
        System.out.println("女觉得你不行！");
    }
}
