package Day3;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/28 16:29
 * @description：//使用线程池
 */
class NumberThread implements Runnable{

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            if (i%2==0){
                System.out.println(Thread.currentThread().getName()+i);
            }
        }
    }
}
class NumberThread1 implements Runnable{

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            if (i%3==0){
                System.out.println(Thread.currentThread().getName()+i);
            }
        }
    }
}
public class TreadPool {
    public static void main(String[] args) {
        //提供所有线程的线程池
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        //按住ctrl加上shift加t可以查看函数，然后发现接口类需要实例化，所以就创建service1才可以进行线程的设置
        ThreadPoolExecutor service1=(ThreadPoolExecutor)executorService;
        //设置属性
        service1.setCorePoolSize(15);
        System.out.println(executorService.getClass());
        //让每个线程跑起来
        executorService.execute(new NumberThread());//适用于runnable
        executorService.execute(new NumberThread1());//适用于runnable
//        executorService.submit();//适合用于callable
        executorService.shutdown();//关闭线程池
    }
}
