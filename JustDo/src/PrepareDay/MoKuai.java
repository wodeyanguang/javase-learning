package PrepareDay;

/**
 * @program: Demo2
 * @description:
 * @author: yan guang
 * @create: 2020-04-19 09:08
 */
class a {
    static {
        System.out.println("静态");
    }

    {
        System.out.println("非静态");
    }

    public void fangfa() {
        System.out.println("非静态方法");
    }

    public static void Ffangfa() {
        System.out.println("静态方法");
    }

    a() {
        System.out.println("构造器");
    }
}

public class MoKuai {
    public static void main(String[] args) {
        new a();
    }
}
