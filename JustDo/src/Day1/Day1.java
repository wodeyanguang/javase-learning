package Day1;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/24 11:34
 * @description：继承的方式创建多线性 setName给线程命名getName获取名字
 * yield()函数就是释放当前线程的cpu占用
 * join()就是临时加入一个线程来占用cpu
 * stop()停止线程
 * sleep()睡眠多长时间
 * isAlive判断线程是否还存在
 * get/setPriority获取和设置线程优先级
 */

class a extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(Thread.currentThread().getName() +Thread.currentThread().getPriority()+ i);
            if (i== 5){
//                this.yield();
                try {
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}

public class Day1 {
    public static void main(String[] args) {
        a b= new a();
        for (int i = 0; i < 5; i++) {
            System.out.println("********++*****");
            if (i==2){
                try {
                    b.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            b.setPriority(Thread.MAX_PRIORITY);
            b.run();
        }
        a c = new a();
        c.setName("线程一");
        c.setPriority(Thread.MIN_PRIORITY);
        c.run();//每个线程只能start一次，不能重复，或者重新创建对象
    }
}
