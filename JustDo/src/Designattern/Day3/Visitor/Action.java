package Designattern.Day3.Visitor;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/21 15:04
 * @description：
 */
public abstract class Action {
    public abstract void getManinfo(Man man);
    public abstract void getWomaninfo(Woman woman);
}
