package JDBC_1.exer;

import JDBC_1.util.JDBCUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Scanner;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/13 14:34
 * @description： 添加操作
 */
public class Exer1test {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("输入用户名: ");
        String name = scanner.next();
        System.out.println("请输入邮箱: ");
        String email = scanner.next();
        System.out.println("青输入生日: ");
        String birthday = scanner.next();//1992-09-08会默认转换

        String sql = "insert into customers(name,email,birth)values(?,?,?)";
        int insertcount = update(sql, name, email, birthday);
        if (insertcount > 0) {
            System.out.println("添加成功！");
        } else {
            System.out.println("添加失败!");
        }
    }

    public static int update(String sql, Object... args) {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            //1.获取连接
            connection = JDBCUtils.getConnection();
            //2.预写sql
            ps = connection.prepareStatement(sql);
            //3.填充占位符
            for (int i = 0; i < args.length; i++) {
                ps.setObject(i + 1, args[i]);//小心参数声明错误
            }
            //4.执行语句
            /*
            如果是一个查询操作有返回结果就返回true，如果是层删改操作就返回falsed
            * */
            ps.execute();
            return ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //5.关闭
            JDBCUtils.closeResourece(connection, ps);
        }
        return 0;
    }
}
