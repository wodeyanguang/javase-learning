package Day7;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/3 16:27
 * @description：
 * 处理流之二:转换流的使用
 * 1.转换流:属于字符流
 * InputstreamReader:将-一个字 节的输入流转换为字符的输入流
 * OutputStreamwriter:将一一个字 符的输出流转换为字节的输出流
 *
 * 2.作用:提供字节流与字符流之间的转换
 *
 * 3.解码:字节字节数组--->字符数组、字符串
 * 编码:字符数组、字符串--->字节字节数组
 *
 * 4.字符集
 * *ASC[I:美国标准信息交换码。
 * 用一个字 节的7位可以表示。.
 * IS08859-1:拉丁码表。欧洲码表
 * 用一个字节的8位表示。
 * GB2312:中国的中文编码表。 最多两个字节编码所有字符
 * GBK:中国的中文编码表升级，融合了更多的中文文字符号。最多两个字节编码
 * Unicode:国际标准码，融合了目前人类使用的所有字符。为每个字符分配唯一的字符码。
 * UTF-8:变长的编码方式，可用1-4个字 节来表示-一个字符。
 */
public class InputOutputReaderTest {
    //将字节转换成字符
    @Test
            public void test1() throws IOException {

        FileInputStream fis = new FileInputStream("hello.txt");
        //输入字节转换成字符的转换流
        InputStreamReader isr = new InputStreamReader(fis,"UTF-8");

        char[] cbuf = new char[20];
        int len;
            while ((len=isr.read(cbuf))!=-1){
                String str =new String(cbuf,0,len);
                System.out.println(str);
            }
        try {
            isr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
