package Day9;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/5 9:25
 * @description： 静态代理举例
 *
 * 特点:代理类和被代理类在编译期间，就确定下来了。
 */
interface ClothFactory{

    void produceCloth();
}

//代理类
class ProxyClothFactory implements ClothFactory{

    private ClothFactory factory;//用被代理类对象进行实例化

    public ProxyClothFactory(ClothFactory factory) {
        this.factory = factory;
    }

    @Override
    public void produceCloth() {
        System.out.println("代理工程做一些准备工作");

        factory.produceCloth();

        System.out.println("代理工厂做一些收尾工作");
    }
}

//被代理类
class NikeClothFactory implements ClothFactory{

    @Override//重写接口中的抽象方法
    public void produceCloth() {
        System.out.println("耐克生产一批运动服");
    }
}
public class StaticProxyTest {
    public static void main(String[] args) {

        NikeClothFactory nikeClothFactory = new NikeClothFactory();
        //创建代理类的对象
        ProxyClothFactory proxyClothFactory = new ProxyClothFactory(nikeClothFactory);
        //执行的是nike的方法
        proxyClothFactory.produceCloth();
    }
}
