package Day8;

import org.junit.Test;

import java.io.IOException;
import java.net.*;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/4 8:25
 * @description： UDP网络编程
 * UDP只管发送，你有没有接收到跟我没有关系
 */
public class UDPTest {
    //发送端
    @Test
    public void sender() throws IOException {
        //1.定义一个socket
        DatagramSocket socket = new DatagramSocket();//万能的F2,这个仅仅是一个确认接接收端的信号，没有数据
        //2.写下要发送的数据
        String str="我是UDP方式发送的导弹";
        byte[] data = str.getBytes();//将字符串转换为字节数组进行发送
        InetAddress inet = InetAddress.getLocalHost();
        //3.数据报用来真正的发送数据,封装一个数据报
        DatagramPacket packet = new DatagramPacket(data,0,data.length,inet,9090);

        socket.send(packet);//发送数据报

        socket.close();
    }
    //接收端
    @Test
    public void receiver() throws IOException {
        //1.定义一个socket
        DatagramSocket soket=new DatagramSocket(9090);
        //2.写下要接收的数据
        byte[] buffer=new byte[100];
        DatagramPacket packet = new DatagramPacket(buffer,0,buffer.length);//用于存放数报
        //3.直接接收
        soket.receive(packet);//直接接受不用调用accpect

        System.out.println(new String(packet.getData(), 0, packet.getLength()));//以string类型来输出数据报数据

        soket.close();
    }
}
