package Day8;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/4 17:21
 * @description： 了解类的加载器
 */
public class ClassLoaderTest {
    @Test
    public void test1(){
        //对于自定义类，使用系统类加戴器进行加戴
        ClassLoader classLoader = ClassLoaderTest. class. getClassLoader();//得到当前类（ClassLoaderTest）的一个加载器
        System. out.println(classLoader);
        //调用系统类加载器的getParent():获取扩展类加载器
        ClassLoader classLoader1 = classLoader. getParent();
        System. out. println(classLoader1);
        //调用扩展类加载器的getParent():获取打展加载器
        ClassLoader classLoader2 = classLoader1. getParent();
        System. out. println(classLoader2);
        //引导类加载器主要负贵加载java的核心类库，无法加载自定义类的。
        ClassLoader classLoader3 = String.class.getClassLoader();
        System.out.println(classLoader3);

    }
    @Test
    public void test2() throws IOException {
        Properties pros = new Properties();//配置文件类
//        此时的文件默认在当前的module下,方式一：
//        FileInputStream fis = new FileInputStream("jdbc.properties");//输入流输入配置文件
//        pros.load(fis);//加载配置文件
//        方式二：路径默认识为module的src下
        ClassLoader classLoader = ClassLoaderTest.class. getClassLoader();

        InputStream is = classLoader.getResourceAsStream("jdbc.properties");//获取本此类的资源转换成字节流，来获取资源
        pros.load(is);//加载

        String user = pros.getProperty("user");
        String password = pros.getProperty("password");

        System.out.println(user+password);
    }

}
