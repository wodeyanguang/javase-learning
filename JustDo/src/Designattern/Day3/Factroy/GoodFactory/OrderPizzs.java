package Designattern.Day3.Factroy.GoodFactory;

import Designattern.Day3.Factroy.GoodFactory.PizzBean.Pizzs;

import java.util.Scanner;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/18 15:06
 * @description： 抽象工厂模式
 * 一般实现类的个数比较多的时候，我们就用这个方法
 * 个数比较少的话可以用抽象类，而不用接口
 * 工厂模式的作用，就是帮你来造对象
 */
public class OrderPizzs {
    private AbFactory abFactory = null;

    public OrderPizzs() {
        setAbFactory();
    }

    public void setAbFactory(){
        while (true) {
            System.out.println("请输入披萨类型");
            String type = getname();
            if ("LD".equalsIgnoreCase(type)){
                abFactory = new LDFactory();
            }else if ("BJ".equalsIgnoreCase(type)){
                abFactory =new BJFactory();
            }else {
                System.out.println("不存在该口味，请重新输入：");
                continue;
            }
            System.out.println("请输入口味");
            String eattype = getname();
            Pizzs pizzs = abFactory.createPizzs(eattype);
            if (pizzs!=null) {
                pizzs.Prepared();
                pizzs.run();
            }else break;
        }
    }

    public String getname(){
        Scanner scanner = new Scanner(System.in);
        String name = scanner.next();
        return name;
    }

    public static void main(String[] args) {
        OrderPizzs orderPizzs = new OrderPizzs();
    }
}
