package Designattern.Day3.Decorator;

import java.util.Scanner;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/20 10:00
 * @description： 装饰者模式
 * new一个对象的时候，用的构造器，构造器返回的就是一个对象
 * 就是哪一个类需要被实例化，我们就加构造器，不用实例化的类，我们可以不用加构造器
 * 就比方说，我们创建甜品的时候想要直接加上已经点过的coffee就要用组合的关系加上去
 * 然后在构造器里面我们需要直接set一下，这样new对象的时候就可以直接有初始值了
 *
 * 抽象类也可以有构造器，虽然自己还是不能实例化，但是这样子类继承的时候就必须要重写了
 */
public class Client {

    public static void main(String[] args) {
        Drink order=null;
        Client client = new Client();
        while (true) {
            System.out.println("请输入想要的品种:");
            String getinfo = client.getinfo();
            if (getinfo.equalsIgnoreCase("adrink")) {
                order = new ADrink();
                int i = order.getcose();
                String j = order.getdscc();
                System.out.println(i + j);
            } else if (getinfo.equalsIgnoreCase("bdrink")) {
                order = new BDrink();
                System.out.println(order.getcose() + order.getdscc());
            } else if (getinfo.equalsIgnoreCase("cdrink")) {
                order = new CDrink();
                System.out.println(order.getcose() + order.getdscc());
            }else if (getinfo.equalsIgnoreCase("milk")){
                order = new Milk(order);
                System.out.println(order.getcose() + order.getdscc());
            }else if (getinfo.equalsIgnoreCase("qiaoke")){
                order = new QiaoKe(order);
                System.out.println(order.getcose() + order.getdscc());
            }else {
                System.out.println("输入错误,订单结束");
                break;
            }

            }
        }

        public String getinfo () {
            Scanner scanner = new Scanner(System.in);
            String next = scanner.next();
            return next;
        }
}
