package Day7;

import org.junit.Test;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/2 21:44
 * @description： 流
 * 一、流的分类:
 * 1.操作数据单位:字节流、字符流
 * 2.数据的流向:输入流、输出流
 * 3.流的角色:节点流、处理流
 * 二、流的体系结构
 * 抽象基类         节点流(或文件流)         缓冲流(处理流的一-种)
 * InputStream    FileInputStream ;    BufferedInputStream
 * OutputStream   File0utputStream     BufferedOutputStream
 * Reader         FileReader           BufferedReader
 * Writer         FileWriter          BufferedWriter
 *
 *结论:
 * 1. 对于文本文件(. txt,.java,.c.cpp)， 使用字符流处理
 * 2. 对于非文本文件(. jpg, . mp3,. mp4,. avi,.doc..ppt....), 使用字节流处理.
 *
 */
public class FileReaderWriterTest {

    /*
    文件读入程序，并且输出到控制台
    如果用throws抛出异常，会到达不了函数底部，所以流没有关闭，不好不适合用throws
    说明点:
1. read()的理解: 返回读入的-一个字符。如果达到文件末尾，返回-1
2.异常的处理:为J保证流资源-定可以执行关闭操作。需要使用try-catch-finally处理,使用try catch也可以执行异常后面的代码

     */
    @Test
    public void test1() {
        FileReader fileReader = null;//读入字符流
        try {
            //实例化
            File file = new File("hello.txt");//相当于当前module
            //制造流
            fileReader = new FileReader(file);
            //读入数据
            //方式一
//        int read = fileReader.read();//返回一个读入的字符，如果是末尾就返回-1
//        while (read!=-1){
//            System.out.print((char)read);//转换成char类型
//            read=fileReader.read();//一个一个读,像指针一样
//        }
            //方式二
            int data;
            while ((data = fileReader.read()) != -1) {
                System.out.println((char) data);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (fileReader != null)
                fileReader.close();//手动关闭流
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //对read()操作升级:使用read的重 载方法
    @Test
    public void test2() {
        FileReader fileReader=null;
        try {
            //1. File类的实例化
            File file = new File("hello.txt");
            //2. FileReader流的实例化
            fileReader=new FileReader(file);
            // 3.读入的操作
            char[] cbuf=new char[5];
            int len;
            while ((len=fileReader.read(cbuf))!=-1){//每次都是在覆盖五个长度的数组，没有增加数组的长度
                for (int i = 0; i <len; i++) {
                    System.out.print(cbuf[i]);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (fileReader!=null){
                try {
                    //4.资源的关闭
                    fileReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 内存中写出数据到文件
     *
     * 说明:
     * 1.输出操作，对应的File可以不存在的。并不会报异常
     * File对应的硬盘中的文件如果不存在，在输出的过程中，会自动创建此文件。
     * File对应的硬盘中的文件如果存在:
     * 如果流使用的构造器是: Filewriter(file,false) / Filewriter(file):对原有文件进行修改
     * 如果流使用的构造器是: FileWriter(file, true):不会对原有文件进行修改，直接继续加
     */
    @Test
    public void test3() throws IOException {
        //1. File类的实例化
        File file =new File("hello1.txt");
        System.out.println(file.getAbsoluteFile());
        //2.写出流的对象实例化
        FileWriter fw=new FileWriter(file);
        //3.写出的操作
        fw.write("I have a boby\n");
        fw.write("you need to have a boby");
        //4.关闭流
        fw.close();
    }

}
