package Day8;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/4 10:09
 * @description：
 */
public class Person extends Object{
    private String name;
    public int age;
    int birthday;

    public int getBirthday() {
        return birthday;
    }

    public void setBirthday(int birthday) {
        this.birthday = birthday;
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
    private Person(String name) {
        this.name = name;
    }

    public Person() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void show(){
        System.out.println("我是一个人");
    }
    public String showNation(String nation){
        System.out.println("我来自"+nation);
        return nation;
    }

    public  static void happy(){
        System.out.println("我很开心");
    }
    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
