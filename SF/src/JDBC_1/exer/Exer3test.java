package JDBC_1.exer;

import JDBC_1.bean.Student;
import JDBC_1.util.JDBCUtils;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Scanner;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/13 15:16
 * @description： 查询操作练习
 */
public class Exer3test {
    public static <T> T getInstance(Class<T> clazz, String sql, Object... args) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = JDBCUtils.getConnection();
            ps = connection.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                ps.setObject(i + 1, args[i]);
            }
            //执行，获得结果集
            rs = ps.executeQuery();
            //获取结果集的元数据
            ResultSetMetaData rsmd = rs.getMetaData();
            //获取字段据个数
            int columnCount = rsmd.getColumnCount();
            if (rs.next()) {
                //获取一个类的对象
                T t = clazz.newInstance();
                for (int i = 0; i < columnCount; i++) {
                    //获取每一个列的列值:通过resultset
                    Object columnValue = rs.getObject(i + 1);
                    //获取列的别名：getcolumnlabel（）推荐使用
                    String columnName = rsmd.getColumnLabel(i + 1);
                    //通过反射将对象指定属性名的属性赋值
                    Field field = clazz.getDeclaredField(columnName);
                    field.setAccessible(true);
                    field.set(t, columnValue);
                }
                return t;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResourece(connection, ps, rs);
        }
        return null;
    }
    public static void main(String[] args) {
        System.out.println("请选择你要输入的类型： ");
        System.out.println("A.准考证号：");
        System.out.println("B.身份证证号：");

        Scanner scanner = new Scanner(System.in);
        String select = scanner.next();
        if ("A".equalsIgnoreCase(select)){
            System.out.println("请输入准考证号： ");
            String examCard = scanner.next();
            String sql="select FlowID flowID,Type type,IDCard,ExamCard examCard,StudentName name,Location location,Grade grade from examstudent where examCard = ?";
            Student student = getInstance(Student.class, sql, examCard);
            if (student!=null){
                System.out.println(student);
            }else {
                System.out.println("您输入的准考证号有误！");
            }
        }else if ("B".equalsIgnoreCase(select)){
            System.out.println("请输入身份证号： ");
            String IDCard = scanner.next();
            String sql="select FlowID flowID,Type type,IDCard,ExamCard examCard,StudentName name,Location location,Grade grade from examstudent where IDCard = ?";
            Student student = getInstance(Student.class, sql, IDCard);
            if (student!=null){
                System.out.println(student);
            }else {
                System.out.println("您输入的身份证号有误！");
            }
        }else {
            System.out.println("您的输入有误，请重新输入： ");
        }
    }
}
























