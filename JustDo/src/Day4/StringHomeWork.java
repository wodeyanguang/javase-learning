package Day4;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/29 8:28
 * @description：
 */
public class StringHomeWork {
    public  static String  reverse(String str,int startindex,int endindex) {
        char[] arr = str.toCharArray();
        for (int x = startindex, y = endindex; x < y; x++, y--) {
            char temp = arr[x];
            arr[x] = arr[y];
            arr[y] = temp;
        }
        return new String(arr);
    }
    public static String reverse1(String str,int startindex,int endindex){
        StringBuffer a=new StringBuffer(str.length());
        a.append(str.substring(0,startindex));//加上开头
        for (int i = endindex; i >=startindex ; i--) {
            a.append(str.charAt(i));//加上中间
        }
        a.append(str.substring(endindex+1));//加上结尾
        return a.toString();//类转换成string
    }
    public static void main(String[] args) {
        String str="abcdefg";
        String str2=reverse(str,1,4);
        System.out.println(str2);
        String str3=reverse1(str,1,3);
        System.out.println(str3);
    }
}
