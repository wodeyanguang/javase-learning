package Day9;

import Day8.Person;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/6 9:00
 * @description： Steam的终止操作
 */
public class StreamAPITest2 {
    //查找与匹配
    @Test
    public void test1() {
        //    allMatch(Predicate p)-检查是否匹配所有元素。练习:是否所有的员.工的年龄都大于18
        Person p1 = new Person("闫广", 20);
        Person p2 = new Person("刘程浩", 19);
        Person p3 = new Person("刘晓刚", 21);
        Person p4 = new Person("王俊", 21);
        Person[] ps = {p1, p2, p3, p4};
        List<Person> personList = Arrays.asList(ps);
        boolean allMatch = personList.stream().allMatch(e -> e.getAge() > 18);
        System.out.println(allMatch);
        //   anyMatch(Predicate p)一 检查是否至少匹配-一个元素。练习:是否存在员工的年级大于18
        boolean anyMatch = personList.stream().anyMatch(e -> e.getAge() > 18);
        System.out.println(anyMatch);
//    noneMatch(Predicate p)一检查是否没有匹配的元素。练习:是否存在员工姓“雷”
        boolean noneMatch = personList.stream().noneMatch(e->e.getAge()==20);//是否没有人年级等于20
        System.out.println(noneMatch);
//    findFirst-返回第-个元素
//    findAny-返回当前流 中的任意元素
//    count-返回流中元素的总个数
//    max(Comparator c)一返回流中最大值
//    练习:返回最高的工资:
//    min(Comparator c)一返回流中最小值
//    练习:返回最低工资的员工
//    forEach(Consumer c)一 内部迭代
    }
    //归约
    @Test
    public void test2(){
//        reduce(T identity, Binaryoperator)- 可以将流中元素反复结合起来，得到-一个值。返回一个T
//        练习1:计算1-10的自然数的和
        List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        Integer sum = integers.stream().reduce(0, Integer::sum);//都是<TTT>类型，输入两个参数返回一个参数
        System.out.println(sum);
//        reduce(BinaryOperator)一可以将流中元素反复结合起来， 得到一个值。返回optional<T>
//        练习2:计算公司所有员工工资的总和
        Person p1 = new Person("闫广", 20);
        Person p2 = new Person("刘程浩", 19);
        Person p3 = new Person("刘晓刚", 21);
        Person p4 = new Person("王俊", 21);
        Person[] ps = {p1, p2, p3, p4};
        List<Person> personList = Arrays.asList(ps);
        Stream<Integer> integerStream = personList.stream().map(Person::getAge);//先获取年龄
//        Optional<Integer> reduce = integerStream.reduce(Integer::sum);
        Optional<Integer> reduce = integerStream.reduce((d1,d2)->d1+d2);//这个方法跟sum一样放俩参数返回一
        System.out.println(reduce);
    }
    //收集
    @Test
    public void test3(){
//        collect(Collector c)-将流转换为其他形式。接收一个Collector 接口的实现，用于给S
//        练习1:查找年龄大于20的员工，结果返回为一个List 或Set
        Person p1 = new Person("闫广", 20);
        Person p2 = new Person("刘程浩", 19);
        Person p3 = new Person("刘晓刚", 21);
        Person p4 = new Person("王俊", 21);
        Person[] ps = {p1, p2, p3, p4};
        List<Person> personList = Arrays.asList(ps);
        List<Person> list = personList.stream().filter(e -> e.getAge() > 20).collect(Collectors.toList());
        list.forEach(System.out::print);
    }
}
