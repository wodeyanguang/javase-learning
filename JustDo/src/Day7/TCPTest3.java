package Day7;

import org.junit.Test;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/3 21:14
 * @description：
 * 实现TCP的网络编程
 * 例题3:从客户端发送文件给服务端，服务端保存到本地并返回“发送成功”给客户端。
 */
public class TCPTest3 {
    @Test//客户端
    public void client() throws IOException {
        //1.造一个socket
        Socket socket=new Socket(InetAddress.getByName("127.0.0.1"),9090);//造一个socket
        //2.获取一个输出流
        OutputStream os = socket.getOutputStream();//造一个输出流
        //3.获取一个输入流，从哪个文件里面去读数据
        FileInputStream fis=new FileInputStream(new File("4.png"));//将图片写入,利用已有的文件
        //4.读数据，写数据
        byte[] buffer=new byte[1024];
        int len;
        while ((len = fis.read(buffer))!=-1){
            os.write(buffer,0,len);//将输入传输出去
        }
        //8.关闭数据的输出,不然上面的循环将会一直在执行
        socket.shutdownOutput();
        //9.接受来自服务器的数据并且显示到控制台上
        InputStream is = socket.getInputStream();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();//输出到控制台
        byte[] bufferr=new byte[20];
        int len1;
        while ((len1=is.read(bufferr))!=-1){
            baos.write(bufferr,0,len1);//将数据存入到数组当中
        }
        System.out.println(baos.toString());
        //5.资源关闭
        fis.close();
        os.close();
        socket.close();
        baos.close();
    }
    @Test//服务器
    public void server() throws IOException {
        //1.造一个socket
        ServerSocket ss = new ServerSocket(9090);
        //2.接收一下客户端的socket
        Socket socket = ss.accept();
        //3.获取客户端的输入流
        InputStream is = socket.getInputStream();
        //4.将数据输入到本地，建立一个本地的输出流
        FileOutputStream fos=new FileOutputStream(new File("7.png"));
        //5.读写过程
        byte[] buffer=new  byte[1024];
        int len;
        while ((len=is.read(buffer))!=-1){//按住F2可以看解释说明
            fos.write(buffer,0,len);
        }
        //7.发送数据给客户端
        OutputStream os = socket.getOutputStream();
        os.write("数据传输完毕！".getBytes());

        //6.资源关闭
        fos.close();
        is.close();
        socket.close();
        ss.close();
        os.close();
    }
}
