package Day6;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/2 19:25
 * @description：File类的-一个对象，代表-一个文件或- 一个文件目录(俗称 : 文件共)
 * 1. File类的一一个对象，代表一个文件或一 个文件目录(俗称: 文件夹)
 * 2. File类声明在java. io包下
 * 3. File类中涉及到关于文件或文件目录的创建、删除、重命名、修改时间、文件大小等方法,
 * 并未涉及到写入或读取文件内容的操作。 如果需要读取或写入文件内容，必须使用I0流来完成。
 * 4.后续File类的对象常会作为参数传递到流的构造器中，指明读取或写入的”终点".
 * 1.如何创建File类的实例
 * <p>
 * <p>
 * 2.
 * 相对路径:相较于某个路径下，指明的路径。
 * 绝对路径:包含盘符在内的文件或文件目录的路径
 */
public class FileTest {
    @Test
    public void test1() {

        //构造器一
        File file = new File("hello.txt");//相对于当前的module路径下
        File file1 = new File("D:\\Demo1\\JustDo\\src\\Day6\\hi.txt");//绝对路径

        System.out.println(file);
        System.out.println(file1);//没有真实生成在硬盘中

        //构造器二
        File file2 = new File("D:\\Demo1", "JustDo");//指定一个文件跟文件目录
        System.out.println(file2);

        //构造器三
        File file3 = new File(file2, "hi.txt");
        System.out.println(file3);
    }

    /*
public String getAbsolutePath(): 获取绝对路径
public String getPath() :获取路径
public String getName() :获取名称
public String getParent(): 获取上层文件目录路径。若无，返回null
public long length() :获取文件长度(即:字节数)。不能获取目录的长度。
public Long LastModified() :获取最后一一次的修改时间，毫秒值
如下的两个方法适用于文件目录：
public String[] list() :获取指定目录下的所有文件或者文件目录的名称数组.
public File[] listFiles() :获取指定目录下的所有文件或者文件目录的File数组
**/
    @Test
    public void test2() {
        File file1 = new File("hello.txt");
        File file2 = new File("D:\\Demo1\\JustDo\\src\\Day6\\hi.txt");

        System.out.println(file1.getAbsoluteFile());
        System.out.println(file1.getPath());
        System.out.println(file1.getName());
        System.out.println(file1.getParent());
        System.out.println(file1.length());
        System.out.println(file1.lastModified());
        System.out.println(new Date(file1.lastModified()));

        System.out.println();
        System.out.println(file2.getAbsoluteFile());
        System.out.println(file2.getPath());
        System.out.println(file2.getName());
        System.out.println(file2.getParent());
        System.out.println(file2.length());
        System.out.println(file2.lastModified());
    }

    @Test
    public void test3() {
        File file = new File("D:\\Demo1\\JustDo\\src");
        String[] list = file.list();
        for (String s : list) {
            System.out.println(s);
        }

        File[] files = file.listFiles();
        for (File file1 : files) {
            System.out.println(file1);
        }
    }

    //    public boolean renameTo(File dest):把文件重命名为指定的文件路径
//    比如: file1. renameTo(file2)为例:
//    要想保证返回true,需要file1在硬盘中是存在的，且file2不能在硬盘 中存在。


    @Test
    public void test4() {
        File file1=new File("hello.txt");
        File file2=new File("D:\\Demo1\\io\\hi.txt");
        //剪贴加重命名
        boolean b = file2.renameTo(file1);
        System.out.println(b);
    }

//    public boolean isDirectory(): 判断是否是文件目录
//    public boolean isFile() :判断是否是文件
//    public boolean exists() :判断是否存在
//    public boolean canRead() :判断是否可读
//    public boolean canWrite() :判断是否可写
//    public boolean isHidden() :判断是否隐藏
        //创建文件
//    public boolean createNewFile() :创建文件。若文件存在，则不创建，返回false
    //创建文件目录
//    public boolean mkdir() :创建文件目录。如果此文件目录存在，就不创建了。如果此文件目录上层目录不存在，也不创建
//    public boolean mkdirs() :创建文件目录。如果_上层文件目录不存在，一并创建
        //删除文件
//public boolean delete(): 删除文件或者文件夹


    @Test
    public void test5() throws IOException {
        File f=new File("hi.txt");
        if (!f.exists()){
            f.createNewFile();
            System.out.println("创建成功");
        }else {
            f.delete();
            System.out.println("删除成功");
        }
    }
    @Test
    public void test6(){
        File file=new File("D:\\Demo1\\io\\io1");
        boolean mkdir = file.mkdir();
        if (mkdir){
            System.out.println("创建成功1");
        }
        File file1=new File("D:\\Demo1\\io\\io2");//会自动造出上级目录
        boolean mkdir1 = file.mkdirs();
        if (mkdir){
            System.out.println("创建成功2");
        }
    }

}
