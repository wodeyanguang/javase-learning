package Designattern.Day3.Adapter.ClassAdpater;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/19 7:52
 * @description： 适配器
 */
public class SPQ extends BSPQ implements JG{
    @Override
    public int Output5v() {
        int v = Output220v();
        v=v/44;
        System.out.println("经过适配器调试之后");
        return v;
    }
}
