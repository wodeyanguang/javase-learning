package Day3;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/28 22:12
 * @description：string函数的不变性
 */
public class StringTest3 {
    String str = new String("good");
    char[] ch = {'t','e', 's', 't' };
public void change(String str, char ch[]) {
        str = "test ok";
        ch[0] = 'b';
        }

    public static void main(String[] args) {
        StringTest3 ex =new StringTest3();
        ex.change(ex.str,ex.ch);//string函数是不可改变的，它不是常量，是一个类，只不过比较特殊，
        // 所以他的地址值没有改变。但是数组的地址值可以改变。
        System.out.println(ex.str);//good
        System.out.println(ex.ch);//best
    }

 }

