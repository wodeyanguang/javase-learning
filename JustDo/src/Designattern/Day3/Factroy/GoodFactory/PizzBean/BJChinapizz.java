package Designattern.Day3.Factroy.GoodFactory.PizzBean;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/18 14:52
 * @description：
 */
public class BJChinapizz extends Pizzs {

    @Override
    public void Prepared() {
        System.out.println("正在加工BJ中国披萨");
    }
}
