package Designattern.Day3.Factroy.GoodFactory;

import Designattern.Day3.Factroy.GoodFactory.PizzBean.BJChinapizz;
import Designattern.Day3.Factroy.GoodFactory.PizzBean.BJUSApizz;
import Designattern.Day3.Factroy.GoodFactory.PizzBean.Pizzs;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/18 15:00
 * @description：
 */
public class LDFactory implements AbFactory{

    private Pizzs pizzs= null;
    @Override
    public Pizzs createPizzs(String type) {
        if ("LDChinapizz".equalsIgnoreCase(type)){
            pizzs = new BJChinapizz();
            pizzs.setname("LDChinapizz");
        }else if ("LDUSApizz".equalsIgnoreCase(type)){
            pizzs = new BJUSApizz();
            pizzs.setname("LDUSApizz");
        }else {
            System.out.println("没有这种类型的披萨");
        }
        return pizzs;
    }
}
