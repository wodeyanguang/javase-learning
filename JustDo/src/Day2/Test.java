package Day2;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/27 17:45
 * @description：交替线程打印
 */

class Number implements Runnable{
    private int number =1;
    @Override
    public void run() {
        while (true){
            synchronized (this){//number是钥匙,一次进来一个
                notify();//唤醒一个线程
            if (number<=20){
                System.out.println(Thread.currentThread().getName()+number);
                number++;

                try {
                    wait();//将线程编程阻塞状态
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }else {
                break;
            }
        }
        }
    }
}
public class Test {
    public static void main(String[] args) {
        Number number=new Number();
        Thread t1=new Thread(number);
        Thread t2=new Thread(number);

        t1.setName("一号");
        t2.setName("二号");

        t1.start();
        t2.start();
    }
}
