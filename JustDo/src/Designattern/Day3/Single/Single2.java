package Designattern.Day3.Single;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/18 8:02
 * @description： 懒汉式（线程不安全）
 * 这里是使用这个方法才进行创建
 * 尽量不用
 */
public class Single2 {
    public static void main(String[] args) {
        Single getinstce = Single.getinstce();
        System.out.println(getinstce.hashCode());
    }
}
class Single{
    private static Single singLe;

    //私有化别人就不能new了
    private Single() {
    }

    public static Single getinstce(){
        if (singLe == null){
            singLe = new Single();
        }
        return singLe;
    }
}
