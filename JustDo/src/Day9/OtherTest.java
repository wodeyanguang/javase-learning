package Day9;

import Day8.Person;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Type;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/5 7:35
 * @description： 反射其他方法
 */
public class OtherTest {
    @Test
    public void test1(){
        Class<Person> clazz = Person.class;
        //getConstructors():获取当前运行时类中声明为public的构造器
        //getDeclaredConstructors():获取当前运行时类中声明的所有的构造器
        Constructor[] constructors = clazz.getConstructors();
        for (Constructor a : constructors) {
            System.out.println(a);
        }
        //获取父类
        Class<? super Person> superclass = clazz.getSuperclass();
        System.out.println(superclass);
        //获取带泛型的父类
        Type genericSuperclass = clazz.getGenericSuperclass();
        System.out.println(genericSuperclass);
    }

}
