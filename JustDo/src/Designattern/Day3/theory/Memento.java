package Designattern.Day3.theory;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/22 17:20
 * @description： 备忘录
 */
public class Memento {
    private String state;

    public Memento(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
