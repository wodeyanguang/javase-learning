package Day9;

import org.junit.Test;

import java.util.Comparator;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/5 14:14
 * @description： Lambda表达式的使用举例
 * <p>
 * 1.举例: (01,02) -> Integer. compare(01,o2);
 * 2.格式:
 * -> :lambda操作符或箭头操作符
 * ->左边: Lambda形参列表 (其实就是接口中的抽象方法的形参列表)
 * ->右边: lambda体 (其实就是重写的抽象方法的方法体)
 * <p>
 * 3. Lambda表达式的使用: (分为6种情况介绍)
 * 总结:
 * ->左边: Lambda形参列表的参数类型可以省略(类型推断);如果Lambda形参列表只有一个参数，其
 * ->右边: Lambda体应该使用一 对{}包裹;如果Lambda体只有一 条执行语句(可能是return语句)，
 *
 * 4. Lambda表达式的本质:作为函数式接口的实例
 *
 * 5.如果一个接口中，只声明了一个抽象方法，则此接口就称为函数式接口;
 * 下面的所有接口都是函数式接口。这样做可以检查它是否是一个函数式接口。
 * 6. |所以以前用匿名实现类表示的现在都可以用Lambda表达式来写。
 *
 * java内置的4大核心函数式接口
 * 消费型接口Consumer<T>
 * void accept(T t)
 * 供给型接口Supplier<T>
 * T get()
 * 函数型接口Function<T, R>
 * R apply(T t)
 * 断定型接口Predicate<T>
 * boolean test(T t)|
 */
public class LambdaTest {
    @Test
    public void test1() {

        Runnable r1 = new Runnable() {
            @Override
            public void run() {
                System.out.println("我爱武汉");
            }
        };
        r1.run();

        System.out.println("-----------------");

        Runnable r2 = () -> System.out.println("我爱北京");

        r2.run();

    }

    @Test
    public void test2() {
        Comparator<Integer> com1 = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return Integer.compare(o1, o2);
            }
        };
        int compare1 = com1.compare(12, 21);
        System.out.println(compare1);
        System.out.println("******************");

        //Lambda表达式
        Comparator<Integer> com2 = (o3, o4) -> Integer.compare(o3, o4);

        int compare2 = com2.compare(4, 3);
        System.out.println(compare2);

        //方法引用
        Comparator<Integer> com3 = Integer::compare;
        int compare3 = com3.compare(2, 2);
        System.out.println(compare3);
    }

}
