package Day7;

import org.junit.Test;

import java.io.*;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/3 14:20
 * @description： 缓冲流的使用
 * BufferedInputStream
 * Buffered0utputS tream
 * BufferedReader
 * BufferedWriter
 *2.作用:提供流的读取、写入的速度
 * 提高读写速度的原因:内部提供了一个缓冲区
 * 3. 处理流，就是“套接”在已有的流的基础上。
 *
 */
public class BuffTest {

    @Test
    public void test1() throws FileNotFoundException {
        BufferedInputStream bis= null;
        BufferedOutputStream bos= null;
        try {
            //造文件
            File srcfile=new File("4.png");
            File desfile=new File("5.png");
            //造流
            //造节点流
            FileInputStream fis=new FileInputStream(srcfile);
            FileOutputStream fos=new FileOutputStream(desfile);
            //造缓冲流
            bis = new BufferedInputStream(fis);
            bos = new BufferedOutputStream(fos);

            //复制与读取写入

            byte[] buff = new byte[30];
            int len;
            while ((len=bis.read(buff))!=-1){
                bos.write(buff,0,len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (bos!=null){
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (bis!=null){
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        //关闭缓存,先关闭外层的流，再关闭内层的流，跟穿衣服一样

        //我们关闭外层，内层自动关闭
//        fos.close();
//        fis.close();
    }
}
