package Designattern.Day3.Iterator;

import java.util.Iterator;
import java.util.List;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/21 20:29
 * @description： 这是专门用来迭代专业的迭代器
 */
public class InfoIterator implements Iterator {
     List<ZY> zyList;
     int index=-1;

    public InfoIterator(List<ZY> zyList) {
        this.zyList = zyList;
    }

    @Override
    public boolean hasNext() {
       if (index>=zyList.size()-1){
           return false;
       }else {
           index++;
           return true;
       }
    }

    @Override
    public Object next() {
        return zyList.get(index);
    }

    @Override
    public void remove() {

    }
}
