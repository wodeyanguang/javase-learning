package Day3;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/29 6:42
 * @description：boolean endswith(String suffix): 测试此字符串是否以指定的后缀结束
 * boolean startsWith(String prefix): 测试此字符串是否以指定的前缀开始
 * boolean startsWith(String prefix, int toffset): 测试此字符串从指定索引开始的子字
 * boolean contains(CharSequence s):当且仅当此字符串包含指定的char值序列时，返回
 * int indexOf(String str): 返回指定子字符串在此字符串中第一次出现处的索引
 * int indexOf(String str, int fromIndex): 返回指定子字符串在此字符串中第一次出 现处古
 * int LastIndexOf(String str): 返回指定子字符串在此字符串中最右边出现处的索引
 * int lastIndexOf(String str, int fromIndex): 返回指定子字符串在此字符串中最后-次
 * 注: index0f和LastIndex0f 方法如果夫找到都是返回-1
 * String replace(char oldChar, char newChar):返回一.个新的字符串，它是
 * 通过用newChar替换此字符串中出现的所有oldChar得到的。
 * String replace(CharSequence target, CharSequence replacement): 使
 * 用指定的字面值替换序列替换此字符串所有匹配字面值目标序列的子字符串。
 * String replaceAll(String regex, String replacement) :使用给定的
 * replacement替换此字符串所有匹配给定的正则表达式的子字符串。
 * String replaceFirst(String regex, String replacement): 使用给定的
 * replacement替换此字符串匹配给定的正则表达式的第一一个 子字符串。
 * boolean matches(String regex):告知此字符串是否匹配给定的正则表达式。
 * StringD] split(String regex):根据给定正则表达式的匹配拆分此字符串。
 * StringD split(String regex, int limit): 根据匹配给定的正则表达式来拆分此
 * 字符串，最多不超过limit个，如果超过了，剩下的全部都放到最后个元素中。
 */
public class StringTest5 {
    public static void main(String[] args) {
        String str1 = "helloworld";
        boolean b1 = str1.endsWith("rld");
        System.out.println(b1);

        boolean b2 = str1.startsWith("ll", 2);
        System.out.println(b2);

        String str2 = "ll";
        System.out.println(str1.contains(str2));

        System.out.println(str1.indexOf("lo"));

        String str3 = str1.replace('h', 'H');
        System.out.println(str3);

        String s4=str1.replace("he","HE");
        System.out.println(s4);

        String t1="12345";
        System.out.println(t1.matches("\\d+"));//判断是否都为数字
    }
}
