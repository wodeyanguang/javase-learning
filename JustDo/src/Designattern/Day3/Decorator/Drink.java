package Designattern.Day3.Decorator;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/20 9:48
 * @description：
 */
public abstract class Drink {
    private String dsc;
    private int cost;

    public String getDsc() {
        return dsc;
    }

    public void setDsc(String dsc) {
        this.dsc = dsc;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public abstract int getcose();

    public abstract String getdscc();
}
