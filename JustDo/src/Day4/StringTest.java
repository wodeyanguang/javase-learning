package Day4;

import java.util.Arrays;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/29 7:38
 * @description：String --> char[]: 调用String的toCharArray()
 * char[] --> String: 调用String的构造器
 * String与byte[]之间的转换
 * String --> byte[]: 调用String的getBytes()
 */
public class StringTest {
    public static void main(String[] args) {
        String a = "abc123";
        char[] a1 = a.toCharArray();
        for (int i = 0; i < a1.length; i++) {
            System.out.println(a1[i]);
        }
        String s2=new String(a1);
        System.out.println(s2);

        byte[] bytes = a.getBytes();
        System.out.println(Arrays.toString(bytes));
    }
}
