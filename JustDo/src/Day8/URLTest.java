package Day8;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/4 8:55
 * @description： URL网络编程
 * 1.URL:统一 资源定位符，对应者互联网的某一 资源地址
 * 2.格式:
 * http://localhost :8080/examples/beauty. jpg?username=Tom
 * 协议    主机名    端口号        资源地址            参数列表
 */
public class URLTest {

    public static void main(String[] args) {
        URL url1= null;
        try {
            //写一个网址，里面有一张图片
            url1 = new URL("http://localhost :8080/examples/beauty. jpg?username=Tom");
            System.out.println(url1.getProtocol());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
//        public String getProtocol( )
//        获取该URL的协议名I
//        public String getHost( )
//        获取该URL的主机名
//        public String getPort( )
//        获取该URL的端口号
//        public String getPath( )
//        获取该URL的文件路径
//        public String getFile( )
//        获取该URL的文件名
//        public String getQuery( )
//        获取该URL的查询名
    }
}
