package JDBC_1;

import org.junit.jupiter.api.Test;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/11 16:58
 * @description：
 */
public class ConnectionTest {
    //方式一：
    @Test
    public void testConnection1() throws SQLException {
        //首先得有一个驱动
        Driver diver = new com.mysql.jdbc.Driver();
        //url相当于一个路线
        String ur1 = "jdbc:mysql://localhost:3306/test";
        //localhost是ip地址
        //test指的是数据库
        //info相当于一个人
        Properties info = new Properties();
        info.setProperty("user", "root");
        info.setProperty("password", "12345678");
        //讲用户名和密码写入
        //可以进行连接了
        Connection conn = diver.connect(ur1, info);

        System.out.println(conn);
    }

    //方式二：利用反射的动态原理来灵活对待各种SQL,尽量不要出现第三方的API
    @Test
    public void testConnection2() throws Exception {
        //使用反射来获取Driver对象，来封装一下
        Class clazz = Class.forName("com.mysql.jdbc.Driver");

        Driver driver = (Driver) clazz.newInstance();
        //提供原油的连接数据库
        String ur1 = "jdbc:mysql://localhost:3306/test";
        Properties info = new Properties();
        info.setProperty("user", "root");
        info.setProperty("password", "12345678");

        Connection connect = driver.connect(ur1, info);

        System.out.println(connect);
    }

    //方式三：使用DriverMananger来替换Driver
    @Test
    public void testConnection3() throws Exception {
        //获取对象
        Class clazz = Class.forName("com.mysql.jdbc.Driver");
        Driver driver = (Driver) clazz.newInstance();

        //获取另外三个基本信息
        String url = "jdbc:mysql://localhost:3306/test";
        String user = "root";
        String password = "12345678";
        //注册驱动
        DriverManager.registerDriver(driver);
        //获取连接
        Connection connection = DriverManager.getConnection(url, user, password);

        System.out.println(connection);
    }
    //方式四：在三的基础上优化：
    @Test
    public void testConnection4() throws Exception{
        //获取三个基本信息
        String url = "jdbc:mysql://localhost:3306/test";
        String user = "root";
        String password = "12345678";

//        //获取对象
//        Class clazz = Class.forName("com.mysql.jdbc.Driver");
//        Driver driver = (Driver) clazz.newInstance();
//        //注册驱动
//        DriverManager.registerDriver(driver);

        //加载Driver，执行了内部的静态代码块儿，自动执行驱动;
        Class.forName("com.mysql.jdbc.Driver");

        //获取连接
        Connection connection = DriverManager.getConnection(url, user, password);
        System.out.println(connection);
    }
    //方式五：将数据库信息放入配置文件，这样是最终版
    /*
    * 这个方案方便更改对数据库的连接，只用更改配置文件中的数据即可
    * 也是一种动态代理
    * */
    @Test
    public void getConnection5() throws Exception {
        //1.读取配置文件信息，定义一个输入流
        InputStream is= ConnectionTest.class.getClassLoader().getResourceAsStream("jdbc.properties");

        Properties pros = new Properties();
        pros.load(is);

        String user = pros.getProperty("user");
        String password = pros.getProperty("password");
        String url = pros.getProperty("url");
        String driverClass = pros.getProperty("driverClass");
        //2.加载驱动
        Class.forName(driverClass);
        //3.获取连接
        Connection conn = DriverManager.getConnection(url,user,password);
        System.out.println(conn);
    }

    @Test
    public void test5again() throws Exception {
        InputStream is = ConnectionTest.class.getClassLoader().getResourceAsStream("jdbc.properties");

        Properties pp = new Properties();
        pp.load(is);

        String user=pp.getProperty("user");
        String password=pp.getProperty("password");
        String url=pp.getProperty("url");
        String driverClass=pp.getProperty("driverClass");

        Class.forName(driverClass);

        Connection connection = DriverManager.getConnection(url, user, password);
        System.out.println(connection);
    }
}




























