package Designattern.Day3.Decorator;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/20 9:57
 * @description：
 */
public class CDrink extends Coffee{
    public CDrink() {
        setCost(3);
        setDsc("这是CCOffee");
    }
}
