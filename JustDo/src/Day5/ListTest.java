package Day5;

import org.junit.Test;

import java.util.ArrayList;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/4/30 11:52
 * @description：/----ColLection接口:单列集合，用来存储一一个- 一个的对象
 * /----List接口: 存储有序的、可重复的数据。I -->“动态”数组
 * /----ArrayList、 LinkedList、Vector三者的异同
 * /----ArrayList:作为ist接口的主要实现类，线程不安全效率高，这个是顺序存储
 * /----LinkedList：底层是双线链表进行存储，对于频繁的插入与删除，这个效率高一些
 * /----Vector:作为list 接口的古老实现类，线程安全，效率低
 * /*
 * 集合的操作ArrayList
 * void add(int index, object ele): 在index位置插λele元素
 * boolean addAll(int index, Collection eles): Mindex位置开始将eles中的所有元素添加进;
 * object get(int index): 获取指定index位置的元素
 * int index0f(0bject obj): 返回obj在集合中首次出现的位置
 * int LastIndex0f(Object obj): 返@obj在当前集合中未次出现的位置
 * object remove(int index): 移除指定index位置的元素，并返回此元素
 * object set(int index, object ele): 设置指定index位置的元素为ele .
 * List sublist(int fromIndex, int toIndex): 返回从fromIndex到toIndex位置的于集合
 * 总结:常用方法
 * 增: add(object obj)
 * 删: remove(int index) / remove(0bject obj)
 * 改: set(int index, object ele)
 * 查: get(int index)
 * 插: add(int index, object ele)
 * 长度: size()
 * 遍历:①Iterator迭代器方式:
 * ②增强for循环
 */
public class ListTest {
    ArrayList arrayList=new ArrayList(10);//构造器里面可以初始化大小，但是会自动扩容

    @Test
    public void test1(){
        System.out.println("单元测试方法");
    }


}
