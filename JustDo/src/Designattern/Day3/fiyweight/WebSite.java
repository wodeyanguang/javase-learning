package Designattern.Day3.fiyweight;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/21 9:30
 * @description：
 */
public abstract class WebSite {
    public abstract void use();
}
