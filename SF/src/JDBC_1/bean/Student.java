package JDBC_1.bean;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/13 15:17
 * @description：
 */
public class Student {
    private int flowID;//流水号
    private int type;//考试类型
    private String IDCard;//身份证
    private String examCard;//准考证号
    private String name;//姓名
    private String location;//城市
    private int grade;//成绩

    public Student() {
        super();
    }

    public Student(int flowID, int type, String IDCard, String name, String location, int grade) {
        this.flowID = flowID;
        this.type = type;
        this.IDCard = IDCard;
        this.name = name;
        this.location = location;
        this.grade = grade;
    }

    public int getFlowID() {
        return flowID;
    }

    public void setFlowID(int flowID) {
        this.flowID = flowID;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getIDCard() {
        return IDCard;
    }

    public void setIDCard(String IDCard) {
        this.IDCard = IDCard;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    @Override
    public String toString() {
        System.out.println("===========查询结果============");
        return "Student" +
                "flowID=" + flowID +
                " \ntype=" + type +
                " \nIDCard='" + IDCard + '\'' +
                " \nname='" + name + '\'' +
                " \nlocation='" + location + '\'' +
                " \ngrade=" + grade
                ;
    }
}
