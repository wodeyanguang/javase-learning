package Designattern.Day2.Part2;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/17 15:51
 * @description：
 */
public class Open implements openTV{
    private PlayTV tv;

    public Open(PlayTV tv) {
        this.tv = tv;
    }

    @Override
    public void open() {//这样其实就是实现了接口的依赖关系
        //通过这个接口调用另一个接口的实现类，然后可以完成那个实现类里面的功能
        this.tv.playTv();
    }
    //下面写的这个更好，可以区分功能
    public void play(){
        this.tv.playTv();
    }

    public void setTv(PlayTV tv) {
        this.tv = tv;
    }
}
