package Designattern.Day3.State;

/**
 * @author Administrator
 *  发放奖品的状态
 */
public class DispenseState extends State {

    //当前活动状态
    RaffleActivity activity;

    public DispenseState(RaffleActivity activity) {
        this.activity = activity;
    }

    @Override
    public void deductMoney() {
        System.out.println("不能扣除积分");
    }

    @Override
    public boolean raffle() {
        System.out.println("不能抽奖");
        return false;
    }

    @Override
    public void dispensePrize() {
        if(activity.getCount() > 0){
            System.out.println("恭喜中奖了！");
            // 改变活动状态为不能抽奖
            activity.setState(activity.getNoRafflleState());
        }else{
            System.out.println("很遗憾奖品发完了！");
            //改变状态为奖品发布完毕
            activity.setState(activity.getDispensOutState());
        }

    }
}
