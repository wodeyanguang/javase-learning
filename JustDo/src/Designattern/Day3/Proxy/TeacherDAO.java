package Designattern.Day3.Proxy;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/21 10:45
 * @description：
 */
public class TeacherDAO implements ITeacherDAO{
    @Override
    public void teach() {
        System.out.println("正在授课");
    }
}
