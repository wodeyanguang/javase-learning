package Designattern.Day3.Adapter.InterAdapter;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/19 9:54
 * @description：
 */
public abstract class Abstrction implements Modthn{
    @Override
    public void function1() {
        System.out.println("重写之前的方法一");
    }

    @Override
    public void function2() {
        System.out.println("方法二也可以调用");
    }

    @Override
    public void function3() {
    }
}
